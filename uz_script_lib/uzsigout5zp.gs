// Script written by Oleg Khimich aka OlegKhim (DSemen), Camille Akhmetzyanov aka kemal, Dmitry Kovaliov aka Дмитрий К, Mikhail Rozov aka RMM, Evgeniy Varvanin aka Varz.
// Organization Trainz UP http://trainzup.com/

include "uzsignalt18.gs"

class uzSigOut5zp isclass uzSignalT18{
	void SetName(string value){
		inherited(value);
		string[] chars=GetChars(value);
		int i;
		for(i=0;i<5;i++){
			if(chars.size()>i and chars[i]!=""){
				SetFXNameText("name"+i, chars[i]);
				SetMeshVisible("ta"+i, true, 0.0);
			}
			else{
				SetFXNameText("name"+i, " ");
				SetMeshVisible("ta"+i, false, 0.0);
			}
		}
	}








//	int speedlim1 = 1;
	int speedlim2 = 0;



/*
	// Создание таблицы HTML
	public string GetDescriptionHTML(void){
		string speedlimtxt;
/ *		if(acaution)
		else{
			if(speedlim2 == 0)
				speedlimtxt = ST.GetString("speed_0");
			else
				speedlimtxt = ST.GetString("speed_poezd_"+speedlim2);
		}

		/ *
		int i;
		if(acaution){
			if(speedlim1 == 1) {
				ret=ret+HTMLWindow.MakeRow(HTMLWindow.MakeCell(ST.GetString("text3"),bgcol)+HTMLWindow.MakeCell(ST.GetString("speed_manevr_1"),bgcol3));
			}
		}
		else{
			ret=ret+HTMLWindow.MakeRow(HTMLWindow.MakeCell(ST.GetString("text1"),bgcol)+HTMLWindow.MakeCell(MakeProperty("speedlim2_"+speedlim2,speedlimtxt),bgcol3));
		}* /
		return ret;
	}*/







/*
	/// Создание ссылок
	void LinkPropertyValue(string p_propertyID){
		if (p_propertyID == "acaution")acaution = !acaution;



		else if (p_propertyID == "speedlim2_0")speedlim2 = 1;
        else if (p_propertyID == "speedlim2_1")speedlim2 = 2;
        else if (p_propertyID == "speedlim2_2")speedlim2 = 3;
        else if (p_propertyID == "speedlim2_3")speedlim2 = 4;
        else if (p_propertyID == "speedlim2_4")speedlim2 = 5;
        else if (p_propertyID == "speedlim2_5")speedlim2 = 0;
		//Изменение состояния изостыка

		else inherited(p_propertyID);
		ChangeSignalEffect();
	}

*/





	// Запись информации в базу данных
	public Soup GetProperties(void){
		Soup soup = inherited();

//		SignalSoup.SetNamedTag("speedlimit1",speedlim1);
		soup.SetNamedTag("speedlimit2",speedlim2);
		return soup;
	}

	// Загрузка информации из базы данных
	public void SetProperties(Soup soup){
		Soup SignalSoup = soup.GetNamedSoup("sigdata");
		if(SignalSoup.CountTags()){
			if(soup.IsLocked()){
				Soup tmp=soup;
				soup=Constructors.NewSoup();
				soup.Copy(tmp);
			}
			int i;
			for(i=0;i<SignalSoup.CountTags();i++){//Пора и от этого избавиться!!!
				string name=SignalSoup.GetIndexedTagName(i);
				soup.SetNamedTag(name, SignalSoup.GetNamedTag(name));
			}
			soup.RemoveNamedTag("sigdata");
		}


		speedlim2 = soup.GetNamedTagAsInt("speedlimit2");
		inherited(soup);
	}



	// зажигание сигналов светофора в зависимости от установленных параметров
	void ChangeSignalEffect(int NextState){
		int SigState = me.GetSignalStateEx();
		me.SetSpeedLimit(0.0);
		blens="";
		state = state & ~(SS_COUNT | SS_MRT | SS_MRT18);

		SetMeshVisible("yellow", false, 0.4);
		SetMeshVisible("red", false, 0.4);
		SetMeshVisible("green", false, 0.4);
		SetMeshVisible("yellow2", false, 0.4);
		SetMeshVisible("white", false, 0.4);
		SetMeshVisible("green_p1", false, 0.4);
		SetMeshVisible("arrow0", false, 0.4);
		SetMeshVisible("arrow1", false, 0.4);

		if (SigState == EX_STOP or SigState == EX_STOP_THEN_CONTINUE){
			SetMeshVisible("red", true, 0.9);
			ALSNcode=CODE_REDYELLOW;
			state = state | SS_COUNT0;
			return;//nothing more to do here
		}
		if(acaution){
			SetMeshVisible("white", true, 0.9);
			switch(speedlim1){
				case 1:SetSpeedLimit(4.25);break;//5.65
				case 2:SetSpeedLimit(6.9);break;
				case 3:SetSpeedLimit(11.1);break;
				case 4:SetSpeedLimit(16.6);break;
				default:SetSpeedLimit(0.0);break;
			}
			ALSNcode=CODE_REDYELLOW;
			state = state | SS_COUNT0;
			return;
		}
		if (NextState & SS_WW) {
			SetMeshVisible("yellow", true, 0.9);
			SetMeshVisible("white", true, 0.9);
			blens="yellow";
			blinker();
			ALSNcode = CODE_YELLOW;
			state = state | SS_COUNT0;
			return;
		}
		if (NextState & SS_ALS) {
			NextState = NextState & ~(SS_MRT | SS_MRT18);
			SetMeshVisible("white", true, 0.9);
		}
		int MRT=CheckMRT(0, 0);
		if(MRT&uzPathBranch.J_MRT){
			SetMeshVisible("yellow2", true, 0.9);
			SetMeshVisible("yellow", true, 0.9);//на отклонение в любом случае два жёлтых
			if(SigState==EX_PROCEED or SigState==EX_ADVANCE_CAUTION){//Если же впереди свободны 2 БУ, то достаточно лишь включить мигалку
				blens="yellow";
				blinker();
			}
			switch(speedlim2){
				case 1:SetSpeedLimit(6.95);break;
				case 2:SetSpeedLimit(11.1);break;
				case 3:SetSpeedLimit(13.95);break;
				case 4:SetSpeedLimit(19.4);break;
				case 5:SetSpeedLimit(22.25);break;
				default:SetSpeedLimit(0.0);break;
			}
			ALSNcode=CODE_YELLOW;
			state = state | SS_COUNT1|SS_MRT;
			return;
		}
		if(MRT&uzPathBranch.J_MRT18){
			SetMeshVisible("green_p1", true, 0.9);
			SetMeshVisible("yellow2", true, 0.9);
			if(SigState==EX_CAUTION){
				SetMeshVisible("yellow", true, 0.9);
				ALSNcode=CODE_YELLOW;
				state = state | SS_COUNT1;
				return;
			}
			if(NextState&SS_MRT)
				blens="yellow";
			else
				blens="green";
			SetMeshVisible(blens, true, 0.9);
			blinker();
			ALSNcode=CODE_GREEN;
			state = state | SS_COUNT2|SS_MRT18;
			return;
		}
		if(korBU==1)SetMeshVisible("arrow0", true, 0.9);
		if(korBU==2)SetMeshVisible("arrow1", true, 0.9);
		if(SigState==EX_CAUTION){
			SetMeshVisible("yellow", true, 0.9);
			ALSNcode=CODE_YELLOW;
			state = state | SS_COUNT1;
			return;
		}
		ALSNcode=CODE_GREEN;
		if(NextState&SS_MRT){
			SetMeshVisible("yellow", true, 0.9);
			blens="yellow";
			blinker();
			state = state | SS_COUNT1;
			return;
		}
		if(NextState&SS_MRT18){
			blens="green";
			SetMeshVisible(blens, true, 0.9);
			blinker();
			state = state | SS_COUNT2;
			return;
		}
		state = state | SS_COUNT3;
		SetMeshVisible("green", true, 0.9);
	}
};