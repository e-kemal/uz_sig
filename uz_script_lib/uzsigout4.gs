// Script written by Oleg Khimich aka OlegKhim (DSemen), Camille Akhmetzyanov aka kemal, Dmitry Kovaliov aka Дмитрий К, Mikhail Rozov aka RMM, Evgeniy Varvanin aka Varz.
// Organization Trainz UP http://trainzup.com/ 

include "uzSignal.gs"

class uzSigOut4 isclass uzSignal{

	void SetName(string value){
		inherited(value);
		string[] chars=GetChars(value);
		int i;
		for(i=0;i<5;i++){
			if(chars.size()>i and chars[i]!=""){
				SetFXNameText("name"+i, chars[i]);
				SetMeshVisible("ta"+i, true, 0.0);
			}
			else{
				SetFXNameText("name"+i, " ");
				SetMeshVisible("ta"+i, false, 0.0);
			}
		}
	}
/*
	public string GetDescriptionHTML(void){
		string acautiontxt;
		string speedlimtxt;
		string bloktxt;
		if(acaution) {
			acautiontxt = ST.GetString("mode_manevr");
			if(speedlim1 == 0)
				speedlimtxt = ST.GetString("speed_0");
			else
				speedlimtxt = ST.GetString("speed_manevr_"+speedlim1);
		}
		else
			acautiontxt = ST.GetString("mode_poezd");
		if(blok)
			bloktxt = ST.GetString("ab4");
		else
			bloktxt = ST.GetString("ab3");

		string ret="<table>";
		string bgcol=ST.GetString("BGCOLOR");
		string bgcol2=ST.GetString("BGCOLOR2");
		string bgcol3=ST.GetString("BGCOLOR3");
		ret=ret+HTMLWindow.MakeRow(HTMLWindow.MakeCell(ST.GetString("text")));
		ret=ret+HTMLWindow.MakeRow(HTMLWindow.MakeCell(ST.GetString("text6"),bgcol)+HTMLWindow.MakeCell(MakeProperty("blok",bloktxt),bgcol));
		ret=ret+HTMLWindow.MakeRow(HTMLWindow.MakeCell(ST.GetString("text2"),bgcol)+HTMLWindow.MakeCell(MakeProperty("acaution",acautiontxt),bgcol2));
		if(acaution)
			ret=ret+HTMLWindow.MakeRow(HTMLWindow.MakeCell(ST.GetString("text3"),bgcol)+HTMLWindow.MakeCell(MakeProperty("speedlim1_"+speedlim1,speedlimtxt),bgcol3));
		ret=ret+"<tr><td "+bgcol+">"+ST.GetString("izo")+"</td><td "+bgcol2+"><a href='live://property/izo'><img src='pic/iz";
		if(izo)ret=ret+"-a";
		ret=ret+".tga' width=24 height=24></a></td></tr>"; //Галочка изостыка в браузере
		ret=ret+"<tr><td "+bgcol+">"+ST.GetString("box")+"</td><td "+bgcol2+"><a href='live://property/box'><img src='pic/scb";
		if(box)ret=ret+"-a";
		ret=ret+".tga' width=24 height=24></a></td></tr>"; //Галочка шкафа СЦБ в браузере
		ret=ret+HTMLWindow.MakeRow(HTMLWindow.MakeCell(ST.GetString("textname"),bgcol)+HTMLWindow.MakeCell(MakeProperty("name",name),bgcol2));
		ret=ret+"</table>";
		return ret;
	}
* /
	public string GetContentViewDetails(void){
		string acautiontxt;
		if(acaution)
			acautiontxt = ST.GetString("mode_manevr");
		else
			acautiontxt = ST.GetString("mode_poezd");

		string ret="<HTML><body>";
		string bgcol=ST.GetString("BGCOLOR");
		string bgcol2=ST.GetString("BGCOLOR2");
		string bgcol3=ST.GetString("BGCOLOR3");
		ret=ret+HTMLWindow.StartTable();
		ret=ret+HTMLWindow.MakeRow(HTMLWindow.MakeCell(ST.GetString("text2"),bgcol)+HTMLWindow.MakeCell(HTMLWindow.MakeLink("live://property/acaution",acautiontxt),bgcol2));
		ret=ret+HTMLWindow.EndTable();
		ret=ret+"</Body></html>";
		return ret;
	}*/

	// Создание имен свойств
	string GetPropertyName(string id){
		return inherited(id);
	}

	// Создание таблиц свойств
	string GetPropertyType(string id){
		return inherited(id);
	}

	/// Создание ссылок
	void LinkPropertyValue(string id){

		if (id == "speedlim1_0")speedlim1 = 1;
		else if (id == "speedlim1_1")speedlim1 = 2;
		else if (id == "speedlim1_2")speedlim1 = 3;
		else if (id == "speedlim1_3")speedlim1 = 4;
		else if (id == "speedlim1_4")speedlim1 = 0;
		else inherited(id);
//		ChangeSignalEffect(GetNextState());
	}

	void SetPropertyValue(string id, string value){
		inherited(id, value);
	}

	public string GetPropertyValue(string id){
		return inherited(id);
	}
/*
	// Запись информации в базу данных
	public Soup GetProperties(void){
		Soup soup = inherited();
		Soup SignalSoup = Constructors.NewSoup();
		soup.SetNamedSoup("sigdata", SignalSoup);

		SignalSoup.SetNamedTag("acaution",acaution);
		SignalSoup.SetNamedTag("blok",blok);
		SignalSoup.SetNamedTag("speedlimit1",speedlim1);
		SignalSoup.SetNamedTag("name",name);
		SignalSoup.SetNamedTag("izo", izo); //Сохранение состояния изостыка
		SignalSoup.SetNamedTag("box", box); //Сохранение состояния шкафа СЦБ
		return soup;
	}

	// Загрузка информации из базы данных
	public void SetProperties(Soup soup){
		inherited(soup);
		Soup SignalSoup = soup.GetNamedSoup("sigdata");

		if(SignalSoup.CountTags()>0){
			acaution = SignalSoup.GetNamedTagAsBool("acaution");
			blok = SignalSoup.GetNamedTagAsBool("blok");
			speedlim1 = SignalSoup.GetNamedTagAsInt("speedlimit1");
		}
		SetName(SignalSoup.GetNamedTag("name"));

		//Восстановление состояния изостыка
		izo = SignalSoup.GetNamedTagAsBool("izo");
		SetMeshVisible("izo", izo, 0.0);

		//Восстановление состояния шкафа СЦБ
		box = SignalSoup.GetNamedTagAsBool("box");
		SetMeshVisible("box", box, 0.0);

		ChangeSignalEffect();
	}
*/
	// зажигание сигналов светофора
	void ChangeSignalEffect(int NextState) {
		int SigState = GetSignalStateEx();
		SetSpeedLimit(0.0);
		blens="";
		state = state & ~(SS_COUNT | SS_MRT | SS_MRT18);
		SetMeshVisible("yellow", false, 0.4);
		SetMeshVisible("red", false, 0.4);
		SetMeshVisible("green4", false, 0.4);
		SetMeshVisible("red4", false, 0.4);
		SetMeshVisible("green", false, 0.4);
		SetMeshVisible("white", false, 0.4);
		SetMeshVisible("arrow0", false, 0.4);
		SetMeshVisible("arrow1", false, 0.4);

		if (SigState == EX_STOP or SigState == EX_STOP_THEN_CONTINUE){
			if(blok)
				SetMeshVisible("red4", true, 0.9);
			else
				SetMeshVisible("red", true, 0.9);
			ALSNcode=CODE_REDYELLOW;
			state = state | SS_COUNT0;
			return;//nothing more to do here
		}
		if(acaution){
			SetMeshVisible("white", true, 0.9);
			switch(speedlim1){
				case 1:SetSpeedLimit(4.25);break;
				case 2:SetSpeedLimit(6.9);break;
				case 3:SetSpeedLimit(11.1);break;
				case 4:SetSpeedLimit(16.6);break;
				default:SetSpeedLimit(0.0);break;
			}
			ALSNcode=CODE_NONE;
			state = state | SS_COUNT0;
			return;
		}
		if (NextState & SS_WW) {
			SetMeshVisible("yellow", true, 0.9);
			SetMeshVisible("white", true, 0.9);
			blens="yellow";
			blinker();
			ALSNcode = CODE_YELLOW;
			state = state | SS_COUNT0;
			return;
		}
		if (NextState & SS_ALS) {
			NextState = NextState & ~(SS_MRT | SS_MRT18);
			SetMeshVisible("white", true, 0.9);
		}
		if(korBU==1)SetMeshVisible("arrow0", true, 0.9);
		if(korBU==2)SetMeshVisible("arrow1", true, 0.9);
		if(SigState==EX_CAUTION){
			SetMeshVisible("yellow", true, 0.9);
			ALSNcode=CODE_YELLOW;
			state = state | SS_COUNT1;
			return;
		}
		ALSNcode=CODE_GREEN;
		if(NextState&SS_MRT){
			SetMeshVisible("yellow", true, 0.9);
			blens="yellow";
			blinker();
			state = state | SS_COUNT1;
			return;
		}
		if(NextState&SS_MRT18){
			if(blok)
				blens="green4";
			else
				blens="green";
			SetMeshVisible(blens, true, 0.9);
			blinker();
			state = state | SS_COUNT2;
			return;
		}
		if(blok and (NextState&SS_COUNT)<SS_COUNT2){
			SetMeshVisible("green4", true, 0.9);
			SetMeshVisible("yellow", true, 0.9);
			state = state | SS_COUNT2;
			return;
		}
		state = state | SS_COUNT3;
		if(blok)
			SetMeshVisible("green4", true, 0.9);
		else
			SetMeshVisible("green", true, 0.9);
	}
	// Инициализация
	public void Init(Asset asset){
		inherited(asset);
	}
};