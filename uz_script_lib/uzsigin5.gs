// Script written by Oleg Khimich aka OlegKhim (DSemen), Camille Akhmetzyanov aka kemal, Dmitry Kovaliov aka ������� �, Mikhail Rozov aka RMM, Evgeniy Varvanin aka Varz.
// Organization Trainz UP http://trainzup.com/

include "uzSignalT.gs"

class uzSigIn5 isclass uzSignalT{

	void SetName(string value){
		inherited(value);
		string[] chars=GetChars(value);
		int i;
		for(i=0;i<5;i++){
			if(chars.size()>i and chars[i]!=""){
				SetFXNameText("name"+i, chars[i]);
				SetMeshVisible("ta"+i, true, 0.0);
			}
			else{
				SetFXNameText("name"+i, " ");
				SetMeshVisible("ta"+i, false, 0.0);
			}
		}
	}



	int speedlim2 = 0;









	// �������� ������� HTML
	public string GetDescriptionHTML(void){
		string acautiontxt;

		string speedlimtxt;
		if(acaution)
			acautiontxt = ST.GetString("mode_manevr");
		else{
			acautiontxt = ST.GetString("mode_poezd");
			if(speedlim2 == 0)
				speedlimtxt = ST.GetString("speed_0");
			else
				speedlimtxt = ST.GetString("speed_poezd_"+speedlim2);
		}


		string ret=inherited()+"<table>";
		string bgcol=ST.GetString("BGCOLOR");
		string bgcol1=ST.GetString("BGCOLOR1");
		string bgcol2=ST.GetString("BGCOLOR2");
		string bgcol3=ST.GetString("BGCOLOR3");


		ret=ret+"<tr><td "+bgcol+">"+ST.GetString("text2")+"</td><td "+bgcol2+"><a href='live://property/acaution'>"+acautiontxt+"</a></td></tr>";
		if(acaution){
			if(speedlim1 == 1)
				ret=ret+"<tr><td "+bgcol+">"+ST.GetString("text3")+"</td><td "+bgcol3+">"+ST.GetString("speed_manevr_1")+"</td></tr>";
		}
		else
			ret=ret+"<tr><td "+bgcol+">"+ST.GetString("text1")+"</td><td "+bgcol3+"><a href='live://property/speedlim2_"+speedlim2+"'>"+speedlimtxt+"</a></td></tr>";
		ret=ret+"</table>";
		return ret;
	}
/*
	public string GetContentViewDetails(void){
		string acautiontxt;
		if(acaution)
			acautiontxt = ST.GetString("mode_manevr");
		else
			acautiontxt = ST.GetString("mode_poezd");

		string ret="<HTML><body>";
		string bgcol=ST.GetString("BGCOLOR");
		string bgcol2=ST.GetString("BGCOLOR2");
		string bgcol3=ST.GetString("BGCOLOR3");
		ret=ret+HTMLWindow.StartTable();
		ret=ret+HTMLWindow.MakeRow(HTMLWindow.MakeCell(ST.GetString("text2"),bgcol)+HTMLWindow.MakeCell(HTMLWindow.MakeLink("live://property/acaution",acautiontxt),bgcol2));
		ret=ret+HTMLWindow.EndTable();
		ret=ret+"</Body></html>";
		return ret;
	}
*/




	// �������� ���� �������
	string GetPropertyName(string id){
//		if(id=="addjunc")return ST.GetString("junc_choice");
//		if(id[,9]=="junction_")return ST.GetString("junc_choice");
		return inherited(id);
	}

	// �������� ������ �������
	string GetPropertyType(string id){
//		if (id=="addjunc" or id[,9]=="junction_")return "list,1";
//		if (id[,8] == "deljunc_")return "link";
//		if (id[,3] == "jL_")return "link";
//		if (id[,3] == "jR_")return "link";
//		if (id[,5] == "jDir_")return "link";
		return inherited(id);
	}

	/// �������� ������
	void LinkPropertyValue(string p_propertyID){
		if (p_propertyID == "speedlim2_0")speedlim2 = 1;
		else if (p_propertyID == "speedlim2_1")speedlim2 = 2;
		else if (p_propertyID == "speedlim2_2")speedlim2 = 3;
		else if (p_propertyID == "speedlim2_3")speedlim2 = 4;
		else if (p_propertyID == "speedlim2_4")speedlim2 = 5;
		else if (p_propertyID == "speedlim2_5")speedlim2 = 0;

		else inherited(p_propertyID);
//		ChangeSignalEffect();
	}



	public string[] GetPropertyElementList(string id){
		return inherited(id);
	}

	void SetPropertyValue(string id, string value, int index){
		inherited(id, value, index);
	}

	void SetPropertyValue(string id, string value){
		inherited(id, value);
	}

	public string GetPropertyValue(string id){
		return inherited(id);
	}

	// ������ ���������� � ���� ������
	public Soup GetProperties(void){
		Soup soup = inherited();



//		soup.SetNamedTag("speedlimit1", speedlim1);
		soup.SetNamedTag("speedlimit2", speedlim2);
		return soup;
	}

	// �������� ���������� �� ���� ������
	public void SetProperties(Soup soup){
//		soup.Log();
		Soup SignalSoup = soup.GetNamedSoup("sigdata");
		if(SignalSoup.CountTags()){
			if(soup.IsLocked()){
				Soup tmp=soup;
				soup=Constructors.NewSoup();
				soup.Copy(tmp);
			}
			int i;
			for(i=0;i<SignalSoup.CountTags();i++){//���� � �� ����� ����������!!!
				string name=SignalSoup.GetIndexedTagName(i);
				soup.SetNamedTag(name, SignalSoup.GetNamedTag(name));
			}
			soup.RemoveNamedTag("sigdata");
		}
//		soup.Log();


//		speedlim1 = soup.GetNamedTagAsInt("speedlimit1");
		speedlim2 = soup.GetNamedTagAsInt("speedlimit2");



		inherited(soup);
//		ChangeSignalEffect(GetNextState());
	}

/*
	// ����� ��������
	void SignalMonitor(Message msg){
		if(msg.major == "Signal")
			if(cast<Signal>msg.src == me) ChangeSignalEffect(GetNextState());

		if(msg.major == "Junction"){
			me.SetSignalState(AUTOMATIC,"");
			ChangeSignalEffect(GetNextState());
		}
	}
*/




	// ��������� �������� ��������� � ����������� �� ������������� ����������
	void ChangeSignalEffect(int NextState) {
		int SigState = me.GetSignalStateEx();
		me.SetSpeedLimit(0.0);
		blens="";
		state = state & ~(SS_COUNT | SS_MRT | SS_MRT18);

		SetMeshVisible("yellow", false, 0.4);
		SetMeshVisible("red", false, 0.4);
		SetMeshVisible("green4", false, 0.4);
		SetMeshVisible("red4", false, 0.4);
		SetMeshVisible("green", false, 0.4);
		SetMeshVisible("yellow2", false, 0.4);
		SetMeshVisible("white", false, 0.4);
		SetMeshVisible("arrow0", false, 0.4);
		SetMeshVisible("arrow1", false, 0.4);

		if (SigState == EX_STOP or SigState == EX_STOP_THEN_CONTINUE){
			if(blok)
				SetMeshVisible("red4", true, 0.9);
			else
				SetMeshVisible("red", true, 0.9);
			ALSNcode=CODE_REDYELLOW;
			state = state | SS_COUNT0;
			return;//nothing more to do here
		}
		if(acaution){
			SetMeshVisible("white", true, 0.9);blens="white";blinker();
			if(blok)
				SetMeshVisible("red4", true, 0.9);
			else
				SetMeshVisible("red", true, 0.9);
			if(speedlim1 == 1)
				me.SetSpeedLimit(5.65);
			ALSNcode=CODE_REDYELLOW;
			state = state | SS_COUNT0;
			return;
		}
		if(isMRT()){
			SetMeshVisible("yellow2", true, 0.9);
			SetMeshVisible("yellow", true, 0.9);//�� ���������� � ����� ������ ��� �����
			if(SigState==EX_PROCEED or SigState==EX_ADVANCE_CAUTION){//���� �� ������� �������� 2 ��, �� ���������� ���� �������� �������
				blens="yellow";
				blinker();
			}
			switch(speedlim2){
				case 1:SetSpeedLimit(6.95);break;
				case 2:SetSpeedLimit(11.1);break;
				case 3:SetSpeedLimit(13.95);break;
				case 4:SetSpeedLimit(19.4);break;
				case 5:SetSpeedLimit(22.25);break;
				default:SetSpeedLimit(0.0);break;
			}
			ALSNcode=CODE_YELLOW;
			state = state | SS_COUNT1|SS_MRT;
			return;
		}
		if(korBU==1)SetMeshVisible("arrow0", true, 0.9);
		if(korBU==2)SetMeshVisible("arrow1", true, 0.9);
		if (SigState==EX_CAUTION){
			SetMeshVisible("yellow", true, 0.9);
			ALSNcode=CODE_YELLOW;
			state = state | SS_COUNT1;
			return;
		}
		ALSNcode=CODE_GREEN;
		if(NextState&SS_MRT){
			SetMeshVisible("yellow", true, 0.9);
			blens="yellow";
			blinker();
			state = state | SS_COUNT1;
			return;
		}
		if(NextState&SS_MRT18){
			if(blok)
				blens="green4";
			else
				blens="green";
			SetMeshVisible(blens, true, 0.9);
			blinker();
			state = state | SS_COUNT2;
			return;
		}
		if (blok and (NextState&SS_COUNT)<SS_COUNT2){
			SetMeshVisible("green4", true, 0.9);
			SetMeshVisible("yellow", true, 0.9);
			state = state | SS_COUNT2;
			return;
		}
		state = state | SS_COUNT3;
		if(blok)
			SetMeshVisible("green4", true, 0.9);
		else
			SetMeshVisible("green", true, 0.9);
	}/*
	// �������������
	public void Init(Asset asset){
		JM=new Junction[0];
		jDir=new int[0];
		inherited(asset);
		AddHandler(me, "Junction", "Toggled", "SignalMonitor");
	}*/
};