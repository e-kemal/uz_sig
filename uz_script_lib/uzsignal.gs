include "Signal.gs"
include "alsn_provider.gs"

class uzSignal isclass Signal, ALSN_Provider{
	Library uzLib;
	StringTable ST;
	Browser mn;
	string name, station;
	Asset self, LibAsset;
	public define int SS_COUNT	= 3;
	public define int SS_COUNT0	= 0;
	public define int SS_COUNT1	= 1;
	public define int SS_COUNT2	= 2;
	public define int SS_COUNT3	= 3;
	public define int SS_MRT	= 4;
	public define int SS_MRT18	= 8;
	public define int SS_WW		= 16;
	public define int SS_ALS	= 32;
	int state=SS_COUNT0;

	public int GetMyState(){
		return state;
	}

	int GetNextState(){
		GSTrackSearch ts=BeginTrackSearch(true);
		while(ts.SearchNextObject()){
			if(!ts.GetFacingRelativeToSearchDirection())continue;
			Signal sig=cast<Signal>ts.GetObject();
			if(!sig)continue;
			if(cast<uzSignal>sig)
				return (cast<uzSignal>sig).GetMyState();
			switch(sig.GetSignalState()){
				case Signal.GREEN:
					return SS_COUNT3;
				case Signal.YELLOW:
					return SS_COUNT1;
				default:
					return SS_COUNT0;
			}
		}
		return 0;
	}

	string MakeProperty(string link, string text){
		string t;
		if (text=="")
			t=ST.GetString("none");
		else
			t=text;
		return HTMLWindow.MakeLink("live://property/"+link,t);
	}

	int type;
	int siding;
	int hold_mainmode;
	bool blok;
	bool predvhod;
	int acaution_type;
	bool acaution;
	bool korBU_enable;
	int korBU;
	int speedlim1 = 0;
	bool freehead;
	int head_rot, krep_rot;
	int head_cnt;
	bool offset_enable;
	float base_offset, offset;
	bool voffset_enable;
	float voffset;
	int ALSNcode;//считаем в дочернем классе, а здесь только кэшируем.
	int ALSNfreq;
	bool izo = false; //Указатель наличия изостыка
	bool izo_pos = true;
	bool box = false; //Указатель наличия шкафа СЦБ
	bool box_pos = true;
	bool permissive = false;
	bool tooltip = true;

	public bool CanDisplayStateEx(int state)
	{
		if (state == Signal.EX_STOP and !permissive)
			return true;
		if (state == Signal.EX_STOP_THEN_CONTINUE and permissive)
			return true;
		if (state == Signal.EX_CAUTION)
			return true;
		if (state == Signal.EX_ADVANCE_CAUTION and blok)
			return true;
		if (state == Signal.EX_PROCEED)
			return true;
		return false;
	}

	public void ChangeSignalEffect(int NextState){}
	public void ChangeSignalEffect(){
		int oldState = state;
		ChangeSignalEffect(GetNextState());
		if (oldState != state) {
			GSObject[] obj_p = new GSObject[1];
			obj_p[0]=me;
			uzLib.LibraryCall("chain_update", null, obj_p);
		}
	}

	bool blinker_started=false;
	string blens="";
	thread void blinker(){
		if(blinker_started)return;
		blinker_started=true;
		bool state;
		while(blens!=""){
			SetMeshVisible(blens, state=!state, 0.5);
			Sleep(1.0);
		}
		blinker_started=false;
	}

	string[] GetChars(string s){
		string[] ret= new string[0];
		ret[0]="";
		int i=0;
		int k=-1;
		bool f=false;

		while(i<s.size()){
			if(!f){
				k++;
				ret[k,k+1]=new string[1];
				ret[k]="";
			}

			string s2=s[i,i+2];
			if(s2>="А"){
				ret[k]=ret[k]+s[i,i+2];
				i++;
			}
			else{
				s2=s2[0,1];

				if(s2=="(")f=true;
				else if(s2==")")f=false;
				else ret[k]=ret[k]+s[i,i+1];
			}
			i++;
		}
		return ret;
	}

	public int GetALSNCode(void){
		return ALSNcode;
	}

	public int GetALSNTypeSignal(void){
		return type;
	}

	public int GetALSNFrequency(void){
		return ALSNfreq;
	}

	public int GetALSNSiding(void){
		return siding;
	}

	public string GetALSNSignalName(void){
		string[] tmp=GetChars(name);
		string ret="";
		int i;
		for(i=0;i<tmp.size();i++)
			ret=ret+tmp[i];
		return ret;
	}

	public string GetALSNStationName(void){
		return station;
	}

	void SetName(string value){
		name=value;
	}

	void SetkorBU(){
		SetMeshVisible("korbu_krep", korBU>0, 0.0);
		SetMeshVisible("korbu_golova0", korBU==1, 0.0);
		SetMeshVisible("korbu_golova1", korBU==2, 0.0);
	}

	void SetHeadRotation(){
		int i;
		for(i=0;i<head_cnt;i++){
			SetMeshOrientation("krep"+i, 0, 0, (krep_rot)*Math.PI/180.0);
			SetMeshOrientation("golova"+i, 0, 0, (head_rot-krep_rot)*Math.PI/180.0);
		}
		if(korBU>0)
			SetMeshOrientation("korbu_krep", 0, 0, (krep_rot)*Math.PI/180.0);
		if(korBU==1)
			SetMeshOrientation("korbu_golova0", 0, 0, (head_rot-krep_rot)*Math.PI/180.0);
		if(korBU==2)
			SetMeshOrientation("korbu_golova1", 0, 0, (head_rot-krep_rot)*Math.PI/180.0);
	}

	void SetIzo(){
		SetMeshVisible("izo", izo, 0.0);
		if (izo) {
			if(izo_pos)
				SetMeshOrientation("izo", 0, 0, 0);
			else
				SetMeshOrientation("izo", 0, 0, Math.PI);
		}
	}

	void SetBox(){
		SetMeshVisible("box", box, 0.0);
		if (box) {
			if(box_pos) {
				SetMeshTranslation("box", 1, 2.2, 0);
				SetMeshOrientation("box", 0, 0, 0);
			}
			else {
				SetMeshTranslation("box", -1 - 2 * base_offset, 2.2, 0);
				SetMeshOrientation("box", 0, 0, Math.PI);
			}
		}
	}

	string GetCntDesctiption(void){
		string acautiontxt;
		string speedlimtxt;

		if(acaution) {
			acautiontxt = ST.GetString("mode_manevr");
			if(speedlim1 == 0)
				speedlimtxt = ST.GetString("speed_0");
			else
				speedlimtxt = ST.GetString("speed_manevr_"+speedlim1);
		}
		else
			acautiontxt = ST.GetString("mode_poezd");

		string ret = "";
		ret = ret + "<tr>";
		ret = ret + "<td>" + ST.GetString("name") + "</td>";
		ret = ret + "<td align=\"center\"><a href='live://property/name'>";
		if (name == "")
			ret = ret + "<img src=\"pic/misc/d.tga\" mouseover=\"pic/misc/da.tga\">";//*/ST.GetString("none");
		else
			ret = ret + name;
		ret = ret + "</a></td>";
		ret = ret + "</tr>";


		if (type & (TYPE_IN|TYPE_ROUTER|TYPE_OUT|TYPE_DIVIDE)){
			ret = ret + "<tr>";
			ret = ret + "<td>" + ST.GetString("station") + "</td>";
			ret = ret + "<td align=\"center\"><a href='live://property/station_name'>";
			if (station == "")
				ret = ret + /*"<img src=\"pic/misc/d.tga\" mouseover=\"pic/misc/da.tga\">";//*/ST.GetString("none");
			else
				ret = ret + station;
			ret = ret + "</a>&nbsp;<a href='live://property/station_create'><img src=\"pic/misc/st_off.tga\" mouseover=\"pic/misc/st_on.tga\">(" + uzLib.LibraryCall("station_count", null, null) + ")</a>&nbsp;";
			ret = ret + "<a href='live://property/station_delete'><img src=\"pic/misc/st-del_off.tga\" mouseover=\"pic/misc/st-del_on.tga\"></a>";
			ret = ret + "</td>";
			ret = ret + "</tr>";
		}

		ret = ret + "<tr>";
		ret = ret + "<td>" + ST.GetString("ab") + "</td>";
		ret = ret + "<td align=\"center\">";
		if (hold_mainmode) {
			ret = ret + "<img src=\"pic/ab/" + hold_mainmode + "_off.tga\">";
		}
		else {
			if (blok)
				ret = ret + "<a href='live://property/blok'><img src=\"pic/ab/4_off.tga\" mouseover=\"pic/ab/4_on.tga\"></a>";
			else
				ret = ret + "<a href='live://property/blok'><img src=\"pic/ab/3_off.tga\" mouseover=\"pic/ab/3_on.tga\"></a>";
		}
		ret = ret + "</td>";
		ret = ret + "</tr>";

		if(HasMesh("ta_pred")) {
			ret = ret + "<tr>";
			ret = ret + "<td>" + ST.GetString("predvhod") + "</td>";
			ret = ret + "<td align=\"center\"><a href='live://property/predvhod'><img src=\"pic/ab/prd";
			if (predvhod) {
				ret = ret + "-a";
			}
			ret = ret + "_off.tga\" mouseover=\"pic/ab/prd";
			if (predvhod) {
				ret = ret + "-a";
			}
			ret = ret + "_on.tga\"></a></td>";
			ret = ret + "</tr>";
		}

		if(acaution_type){
			ret = ret + "<tr>";
			ret = ret + "<td>" + ST.GetString("mode") + "</td>";
			ret = ret + "<td align=\"center\">";
			ret = ret + "<a href='live://property/acaution'>";
			if (acaution)
				ret = ret + "<img src=\"pic/ab/ma_off.tga\" mouseover=\"pic/ab/ma_on.tga\">";//ST.GetString("mode_manevr");
			else
				ret = ret + "<img src=\"pic/ab/po_off.tga\" mouseover=\"pic/ab/po_on.tga\">";//ST.GetString("mode_poezd");
			ret = ret + "</a></td>";
			ret = ret + "</tr>";

//			ret=ret+MakeProperty("speedlim1_"+speedlim1, speedlim1)+"<br>";
		}

		if (korBU_enable) {
			ret = ret + "<tr>";
			ret = ret + "<td>" + ST.GetString("kor_bu") + "</td>";
			ret = ret + "<td align=\"center\">";
			ret = ret + "<a href='live://property/korBU0'><img src=\"pic/misc/btn_rem.tga\" mouseover=\"pic/misc/btn_rem_over.tga\"></a>&nbsp;";
			ret = ret + "<a href='live://property/korBU2'><img src=\"pic/object/2kb";
			if (korBU == 2) {
				ret = ret + "-a";
			}
			ret = ret + "_off.tga\" mouseover=\"pic/object/2kb";
			if (korBU == 2) {
				ret = ret + "-a";
			}
			ret = ret + "_on.tga\"></a>&nbsp;";
			ret = ret + "<a href='live://property/korBU1'><img src=\"pic/object/kb";
			if (korBU == 1) {
				ret = ret + "-a";
			}
			ret = ret + "_off.tga\" mouseover=\"pic/object/kb";
			if (korBU == 1) {
				ret = ret + "-a";
			}
			ret = ret + "_on.tga\"></a>";
			ret = ret + "</td>";
			ret = ret + "</tr>";
		}

		ret = ret + "<tr>";
		ret = ret + "<td>" + ST.GetString("alsn") + "</td>";
		ret = ret + "<td align=\"center\">";
		ret = ret + "<a href='live://property/freq_0'>" + HTMLWindow.RadioButton("", (ALSNfreq&7)==0) + ST.GetString("alsn_0") + "</a><br>";
		ret = ret + "<a href='live://property/freq_25'>" + HTMLWindow.RadioButton("", ALSNfreq&FREQ_ALSN25) + ST.GetString("alsn_25") + "</a><br>";
		ret = ret + "<a href='live://property/freq_50'>" + HTMLWindow.RadioButton("", ALSNfreq&FREQ_ALSN50) + ST.GetString("alsn_50") + "</a><br>";
		ret = ret + "<a href='live://property/freq_75'>" + HTMLWindow.RadioButton("", ALSNfreq&FREQ_ALSN75) + ST.GetString("alsn_75") + "</a><br>";
		ret = ret + "<a href='live://property/freq_en'>" + HTMLWindow.CheckBox("", ALSNfreq&FREQ_ALSNEN) + ST.GetString("alsn_en") + "</a>";
		ret = ret + "</td>";
		ret = ret + "</tr>";


		if (type & (TYPE_IN|TYPE_ROUTER|TYPE_OUT|TYPE_DIVIDE)) {
			ret = ret + "<tr>";
			ret = ret + "<td>" + ST.GetString("") + "</td>";
			ret = ret + "<td align=\"center\">";
			ret = ret + "<a href='live://property/siding_to'>" + HTMLWindow.CheckBox("", siding&SIDING_TO) + "SIDING_TO</a>";		
			if(type&TYPE_IN)
				ret=ret+"<br><a href='live://property/siding_from'>"+HTMLWindow.CheckBox("", siding&SIDING_FROM)+"SIDING_FROM</a>";
			ret = ret + "</td>";
			ret = ret + "</tr>";
		}

		if (freehead) {
			ret = ret + "<tr>";
			ret = ret + "<td>" + ST.GetString("krep_rot") + "</td>";
			ret = ret + "<td><a href='live://property/krep_rot'><img src=\"pic/offset/rot_off.tga\" mouseover=\"pic/offset/rot_on.tga\">" + krep_rot + "</a></td>";
			ret = ret + "</tr>";
			ret = ret + "<tr>";
			ret = ret + "<td colspan=\"2\" align=\"center\">";
			ret = ret + "<a href='live://property/krep_rot=-65'>-65</a>&nbsp;";
			ret = ret + "<a href='live://property/krep_rot=-55'>-55</a>&nbsp;";
			ret = ret + "<a href='live://property/krep_rot=-45'>-45</a>&nbsp;";
			ret = ret + "<a href='live://property/krep_rot=-35'>-35</a>&nbsp;";
			ret = ret + "<a href='live://property/krep_rot=0'>0</a>&nbsp;";
			ret = ret + "<a href='live://property/krep_rot=35'>35</a>&nbsp;";
			ret = ret + "<a href='live://property/krep_rot=45'>45</a>&nbsp;";
			ret = ret + "<a href='live://property/krep_rot=55'>55</a>&nbsp;";
			ret = ret + "<a href='live://property/krep_rot=65'>65</a>";
			ret = ret + "</td>";
			ret = ret + "</tr>";

			ret = ret + "<tr>";
			ret = ret + "<td>" + ST.GetString("head_rot") + "</td>";
			ret = ret + "<td><a href='live://property/head_rot'><img src=\"pic/offset/pg_off.tga\" mouseover=\"pic/offset/pg_on.tga\">" + head_rot + "</a></td>";
			ret = ret + "</tr>";
			ret = ret + "<tr>";
			ret = ret + "<td colspan=\"2\" align=\"center\">";
			ret = ret + "<a href='live://property/head_rot=-9'>-9</a>&nbsp;";
			ret = ret + "<a href='live://property/head_rot=-7'>-7</a>&nbsp;";
			ret = ret + "<a href='live://property/head_rot=-5'>-5</a>&nbsp;";
			ret = ret + "<a href='live://property/head_rot=0'>0</a>&nbsp;";
			ret = ret + "<a href='live://property/head_rot=5'>5</a>&nbsp;";
			ret = ret + "<a href='live://property/head_rot=7'>7</a>&nbsp;";
			ret = ret + "<a href='live://property/head_rot=9'>9</a>";
			ret = ret + "</td>";
			ret = ret + "</tr>";
		}

		if (offset_enable) {
			ret = ret + "<tr>";
			ret = ret + "<td>" + ST.GetString("offset") + "</td>";
			ret = ret + "<td><a href='live://property/offset'><img src=\"pic/offset/up-s_off.tga\" mouseover=\"pic/offset/up-s_on.tga\">" + offset + "</a></td>";
			ret = ret + "</tr>";
			ret = ret + "<tr>";
			ret = ret + "<td colspan=\"2\" align=\"center\">";
			ret = ret + "<a href='live://property/offset=-3.2'>-3.2</a>&nbsp;";
			ret = ret + "<a href='live://property/offset=-2.65'>-2.65</a>&nbsp;";
			ret = ret + "<a href='live://property/offset=-2.5'>-2.5</a>&nbsp;";
			ret = ret + "<a href='live://property/offset=2.5'>2.5</a>&nbsp;";
			ret = ret + "<a href='live://property/offset=2.65'>2.65</a>&nbsp;";
			ret = ret + "<a href='live://property/offset=2.85'>2.85</a>&nbsp;";
			ret = ret + "<a href='live://property/offset=3.2'>3.2</a>";
			ret = ret + "</td>";
			ret = ret + "</tr>";
		}

		if (voffset_enable) {
			ret = ret + "<tr>";
			ret = ret + "<td>" + ST.GetString("voffset") + "</td>";
			ret = ret + "<td><a href='live://property/voffset'><img src=\"pic/offset/up-d_off.tga\" mouseover=\"pic/offset/up-d_on.tga\">" + voffset + "</a></td>";
			ret = ret + "</tr>";
		}

		if (HasMesh("izo")) {
			ret = ret + "<tr>";
			ret = ret + "<td>" + ST.GetString("izo") + "</td>";
			ret = ret + "<td align=\"center\">";
			ret = ret + "<a href='live://property/izo_l'>";
			if (izo and !izo_pos) {
				ret = ret + "<img src=\"pic/object/ar-l_off.tga\" mouseover=\"pic/object/ar-l_on.tga\">";
			}
			else {
				ret = ret + "<img src=\"pic/object/iz_off.tga\" mouseover=\"pic/object/iz_on.tga\">";
			}
			ret = ret + "</a>&nbsp;<a href='live://property/izo_n'>";
			if (!izo) {
				ret = ret + "<img src=\"pic/misc/btn_rem.tga\" mouseover=\"pic/misc/btn_rem_over.tga\">";
			}
			else {
				ret = ret + "<img src=\"pic/misc/btn_rem.tga\" mouseover=\"pic/misc/btn_rem_over.tga\">";
			}
			ret = ret + "</a>&nbsp;<a href='live://property/izo_r'>";
			if (izo and izo_pos) {
				ret = ret + "<img src=\"pic/object/ar-r_off.tga\" mouseover=\"pic/object/ar-r_on.tga\">";
			}
			else {
				ret = ret + "<img src=\"pic/object/iz_off.tga\" mouseover=\"pic/object/iz_on.tga\">";
			}
			ret = ret + "</a>";
			ret = ret + "</td>";
			ret = ret + "</tr>";
		}

		if (HasMesh("box")) {
			ret = ret + "<tr>";
			ret = ret + "<td>" + ST.GetString("box") + "</td>";
			ret = ret + "<td align=\"center\">";
			ret = ret + "<a href='live://property/box_l'>";
			if (box and !box_pos) {
				ret = ret + "<img src=\"pic/object/ar-l_off.tga\" mouseover=\"pic/object/ar-l_on.tga\">";
			}
			else {
				ret = ret + "<img src=\"pic/object/scb_off.tga\" mouseover=\"pic/object/scb_on.tga\">";
			}
			ret = ret + "</a>&nbsp;";
			ret = ret + "<a href='live://property/box_n'>";
			if (!box) {
				ret = ret + "<img src=\"pic/misc/btn_rem.tga\" mouseover=\"pic/misc/btn_rem_over.tga\">";
			}
			else {
				ret = ret + "<img src=\"pic/misc/btn_rem.tga\" mouseover=\"pic/misc/btn_rem_over.tga\">";
			}
			ret = ret + "</a>&nbsp;";
			ret = ret + "<a href='live://property/box_r'>";
			if (box and box_pos) {
				ret = ret + "<img src=\"pic/object/ar-r_off.tga\" mouseover=\"pic/object/ar-r_on.tga\">";
			}
			else {
				ret = ret + "<img src=\"pic/object/scb_off.tga\" mouseover=\"pic/object/scb_on.tga\">";
			}
			ret = ret + "</a>";
			ret = ret + "</td>";
			ret = ret + "</tr>";
		}

		if (HasMesh("permissive")) {
			ret = ret + "<tr>";
			ret = ret + "<td>" + ST.GetString("permissive") + "</td>";
			ret = ret + "<td align=\"center\"><a href='live://property/permissive'><img src=\"pic/object/znak";
			if (permissive) {
				ret = ret + "-a";
			}
			ret = ret + "_off.tga\" mouseover=\"pic/object/znak";
			if (permissive) {
				ret = ret + "-a";
			}
			ret = ret + "_on.tga\"></a></td>";
			ret = ret + "</tr>";

			if (predvhod and permissive) {
				ret = ret + "<tr>";
				ret = ret + "<td colspan=\"2\" align=\"center\">";
				ret = ret + "<font color=\"#ff0000\">" + ST.GetString("permissive-predvhod") + "</font>";
				ret = ret + "</td>";
				ret = ret + "</tr>";
			}
		}

		if (type & (TYPE_IN|TYPE_ROUTER|TYPE_OUT|TYPE_DIVIDE|TYPE_PASSING)) {
			ret = ret + "<tr>";
			ret = ret + "<td>" + ST.GetString("ww") + "</td>";
			ret = ret + "<td align=\"center\"><a href='live://property/mrww'>" + HTMLWindow.CheckBox("", state & SS_WW) + "</a></td>";
			ret = ret + "</tr>";

			ret = ret + "<tr>";
			ret = ret + "<td>" + ST.GetString("also") + "</td>";
			ret = ret + "<td align=\"center\"><a href='live://property/mrals'>" + HTMLWindow.CheckBox("", state & SS_ALS) + "</a></td>";
			ret = ret + "</tr>";
		}
		return ret;
	}

	public string GetDescriptionHTML(void){
		string[] stringParam = new string[1];
		GSObject[] objectParam = new GSObject[1];
		uzLib.LibraryCall("get_language", stringParam, objectParam);
		if(objectParam and objectParam[0] and cast<StringTable>objectParam[0])
			ST = cast<StringTable>objectParam[0];
		tooltip = stringParam[0] != "notooltip";
		string ret = "<table width=\"100%\">";
		ret = ret + "<tr>";
		ret = ret + "<td><img src=\"pic/misc/uz.tga\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font size=\"4\"><b>" + ST.GetString("lable") + "</b></font></td>";
		ret = ret + "<td align=\"center\"><a href='live://property/language'>" + ST.GetString("lngimg") + "</a></td>";
		ret = ret + "</tr>";

		ret = ret + GetCntDesctiption();

		ret = ret + "<tr>";
		ret = ret + "<td>" + ST.GetString("tooltip") + "</td>";
		ret = ret + "<td><a href='live://property/tooltip'>";
		if (tooltip)
			ret=ret+"<img src=\"pic/misc/pg.tga\" mouseover=\"pic/misc/pgv.tga\">";
		else
			ret=ret+"<img src=\"pic/misc/pd.tga\" mouseover=\"pic/misc/pdv.tga\">";
		ret = ret + "</a></td>";
		ret = ret + "</tr>";
		ret = ret + "</table>";
		return ret;
	}

	public void PropertyBrowserRefresh(Browser browser){
		string html = GetDescriptionHTML();

		browser.LoadHTMLString(LibAsset, html);

		if (m_propertyObjectHandler)
			m_propertyObjectHandler.RefreshBrowser(browser);
	}

	public string GetCntViewDetails(void){
		string[] chars = GetChars(name);
		int i;
		string ret = "<tr><td>" + ST.GetString("name") + "</td><td>";
		if (name == "")
			ret = ret + ST.GetString("none");
		else
			for (i=0; i < chars.size(); i++)
				ret = ret + chars[i];
		if (station != "")
			ret = ret + " @ " + station;
		ret=ret+"</td></tr>";

		if(acaution_type){
			ret = ret + "<tr>";
			ret = ret + "<td>" + ST.GetString("mode") + "</td>";
			ret = ret + "<td align=\"center\">";
			ret = ret + "<a href='live://property/acaution'>";
			if (acaution)
				ret = ret + ST.GetString("mode_manevr");
			else
				ret = ret + ST.GetString("mode_poezd");
			ret = ret + "</a></td>";
			ret = ret + "</tr>";
		}

		return ret;
	}

	public string GetContentViewDetails(void){
		string[] stringParam = new string[1];
		GSObject[] objectParam = new GSObject[1];
		uzLib.LibraryCall("get_language", stringParam, objectParam);
		if(objectParam and objectParam[0] and cast<StringTable>objectParam[0])
			ST = cast<StringTable>objectParam[0];
		tooltip = stringParam[0] != "notooltip";
		string ret = "<table width=\"100%\"><tr><td colspan=\"2\"><img src=\"pic/misc/uz.tga\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font size=\"4\"><b>"+ST.GetString("lable")+"</b></font></td></tr>"+GetCntViewDetails()+"</table>";
		return ret;
	}

	thread void ShowBrowser(void){
		Message msg;
		wait(){
			on "Browser-URL","",msg:
				if(msg.src==mn and msg.minor[0,16]=="live://property/"){
					LinkPropertyValue(msg.minor[16,]);
					mn.LoadHTMLString(self, GetContentViewDetails());
				}
				continue;
			on "Browser-Closed","",msg:
				if (msg.src!=mn)continue;
		}
		mn=null;
	}

	void ViewDetails(Message msg){
		mn=Constructors.NewBrowser();
		mn.LoadHTMLString(self, GetContentViewDetails());
		mn.SetWindowRect(100,100,350,150);
		ShowBrowser();
	}

	string GetPropertyName(string id){
		if(id=="name")return ST.GetString("textname");
		return inherited(id);
	}

	// Создание таблиц свойств
	string GetPropertyType(string id){
		if(id=="tooltip")return "link";
		if(id=="name")return "string,0,8";
		if(id=="station_name")return "list,1";
		if(id=="station_create")return "string,0,100";
		if(id=="station_delete")return "link";
		if(id=="acaution")return "link";
		if(id=="blok")return "link";
		if(id=="predvhod")return "link";
		if(id[,5]=="korBU")return "link";
		if(id=="head_rot")return "int,-180,180,1";
		if(id[,9]=="head_rot=")return "link";
		if(id=="krep_rot")return "int,-180,180,1";
		if(id[,9]=="krep_rot=")return "link";
		if(id=="offset")return "float,-10,10,0.05";
		if(id[,7]=="offset=")return "link";
		if(id=="voffset")return "float,-10,10,0.05";
		if(id[,10]=="speedlim1_")return "link";
		if(id[,10]=="speedlim2_")return "link";
		if(id[,3]=="izo")return "link";
		if(id[,3]=="box")return "link";
		if(id=="permissive")return "link";
		if(id=="mrww")return "link";
		if(id=="mrals")return "link";
		if(id[,5]=="freq_")return "link";
		if(id[,7]=="siding_")return "link";
		return inherited(id);
	}

	void LinkPropertyValue(string id){
		if(id=="station_delete"){
			string[] obj_p=new string[1];
			obj_p[0]=station+"";

			uzLib.LibraryCall("delete_station",obj_p,null);
			obj_p[0]=null;
		}
		else if(id=="acaution")acaution = (!acaution and acaution_type);
		else if(id=="blok")blok = (!blok and hold_mainmode!=3);
		else if(id=="predvhod"){
			predvhod=(!predvhod and HasMesh("ta_pred"));
			SetMeshVisible("ta_pred", predvhod, 0.0);
			SetName(name);
		}
		else if(id=="mrww")
			state = state ^ SS_WW;
		else if(id=="mrals")
			state = state ^ SS_ALS;
		else if(id=="siding_to"){
			if(!(siding&SIDING_TO) and type&(TYPE_IN|TYPE_ROUTER|TYPE_OUT|TYPE_DIVIDE))
				siding=siding|SIDING_TO;
			else
				siding=siding&~SIDING_TO;
		}
		else if(id=="siding_from"){
			if(!(siding&SIDING_FROM) and type&TYPE_IN)
				siding=siding|SIDING_FROM;
			else
				siding=siding&~SIDING_FROM;
		}
		else if(id == "korBU0") {
			korBU = 0;
			SetkorBU();
		}
		else if(id=="korBU1"){
			if(korBU==1 or !korBU_enable)korBU=0;
			else korBU=1;
			SetkorBU();
		}
		else if(id=="korBU2"){
			if(korBU==2 or !korBU_enable)korBU=0;
			else korBU=2;
			SetkorBU();
		}
		else if(id[,9]=="head_rot=" and freehead){
			head_rot = Str.ToInt(id[9,]);
			SetHeadRotation();
		}
		else if(id[,9]=="krep_rot=" and freehead){
			krep_rot = Str.ToInt(id[9,]);
			SetHeadRotation();
		}
		else if(id[,7]=="offset=" and offset_enable){
			offset=Str.ToFloat(id[7,]);
			SetMeshTranslation("default", offset-base_offset, 0, voffset);
		}
		else if(id=="izo"){
			izo=(!izo and HasMesh("izo"));
			SetIzo();
		}
		else if(id=="izo_n"){
			izo = false;
			SetIzo();
		}
		else if(id=="izo_l"){
			izo = HasMesh("izo");
			izo_pos = false;
			SetIzo();
		}
		else if(id=="izo_r"){
			izo = HasMesh("izo");
			izo_pos = true;
			SetIzo();
		}
		else if(id=="box"){
			box=(!box and HasMesh("box"));
			SetBox();
		}
		else if(id=="box_n"){
			box = false;
			SetBox();
		}
		else if(id=="box_l"){
			box = HasMesh("box");
			box_pos = false;
			SetBox();
		}
		else if(id=="box_r"){
			box = HasMesh("box");
			box_pos = true;
			SetBox();
		}
		else if(id=="permissive"){
			permissive=(!permissive and HasMesh("permissive"));
			SetMeshVisible("permissive", permissive, 0.0);
		}
		else if(id=="freq_0")
			ALSNfreq=ALSNfreq&~7;
		else if(id=="freq_25")
			ALSNfreq=(ALSNfreq&~7)|FREQ_ALSN25;
		else if(id=="freq_50")
			ALSNfreq=(ALSNfreq&~7)|FREQ_ALSN50;
		else if(id=="freq_75")
			ALSNfreq=(ALSNfreq&~7)|FREQ_ALSN75;
		else if(id=="freq_en")
			ALSNfreq=ALSNfreq^FREQ_ALSNEN;
		else if(id=="tooltip")
			uzLib.LibraryCall("tooltip_toggle", null, null);
		ChangeSignalEffect();
	}

	void SetPropertyValue(string id, string value){
		if(id=="name")SetName(value);
		else if(id=="station_create"){
			station=value;

			string[] obj_p=new string[1];
			obj_p[0]=station+"";

			if(station != " ")
				uzLib.LibraryCall("add_station",obj_p,null);
			obj_p[0]=null;
		}
		else inherited(id, value);
	}

	void SetPropertyValue(string id, int value){
		if(id=="head_rot" and freehead){
			head_rot=value;
			SetHeadRotation();
		}
		else if(id=="krep_rot" and freehead){
			krep_rot=value;
			SetHeadRotation();
		}
		else inherited(id, value);
	}

	void SetPropertyValue(string id, float value){
		if(id=="offset" and offset_enable){
			offset=value;
			SetMeshTranslation("default", offset-base_offset, 0, voffset);
		}
		else if(id=="voffset" and voffset_enable){
			voffset=value;
			SetMeshTranslation("default", offset-base_offset, 0, voffset);
		}
		else inherited(id, value);
	}

	public string[] GetPropertyElementList(string id){
		if(id == "station_name"){
			string[] ret;
			string[] obj_p=new string[1];
			obj_p[0]=station+"";

			int N = Str.ToInt(uzLib.LibraryCall("station_list", obj_p, null));

			ret= new string[N];
			int i;
			for(i=0;i<N;i++)
				ret[i]=obj_p[i]+"";
			return ret;
		}
		return inherited(id);
	}

	void SetPropertyValue(string id, string value, int index){
		if(id=="station_name"){
			station=value;

			string[] obj_p=new string[1];
			obj_p[0]=station;
			uzLib.LibraryCall("station_edited_set", obj_p, null);
			obj_p[0]=null;
		}
		else
			inherited(id,value,index);
	}

	public string GetPropertyValue(string id){
		if(id=="name")return name;
		if(id=="station_name")return station;
		if(id=="head_rot")return (string)head_rot;
		if(id=="krep_rot")return (string)krep_rot;
		if(id=="offset")return (string)offset;
		if(id=="voffset")return (string)voffset;
		return inherited(id);
	}

	public Soup GetProperties(void){
		Soup soup = inherited();

		soup.SetNamedTag("ALSNfreq", ALSNfreq);
		soup.SetNamedTag("siding", siding);
		if(acaution_type)
			soup.SetNamedTag("acaution", acaution);
		if(!hold_mainmode)
			soup.SetNamedTag("blok", blok);
		if(HasMesh("ta_pred"))
			soup.SetNamedTag("predvhod", predvhod);
		if(korBU_enable)
			soup.SetNamedTag("korBU", korBU);
		if(freehead){
			soup.SetNamedTag("head_rot", head_rot);
			soup.SetNamedTag("krep_rot", krep_rot);
		}
		if(offset_enable)
			soup.SetNamedTag("offset", offset);
		if(voffset_enable)
			soup.SetNamedTag("voffset", voffset);
		soup.SetNamedTag("speedlimit1", speedlim1);
		soup.SetNamedTag("name", name);
		soup.SetNamedTag("station", station);
		if(HasMesh("izo")) {
			soup.SetNamedTag("izo", izo); //Сохранение состояния изостыка
			soup.SetNamedTag("izo_pos", izo_pos); //Сохранение состояния изостыка
		}
		if(HasMesh("box")) {
			soup.SetNamedTag("box", box); //Сохранение состояния шкафа СЦБ
			soup.SetNamedTag("box_pos", box_pos); //Сохранение состояния шкафа СЦБ
		}
		if(HasMesh("permissive"))
			soup.SetNamedTag("permissive", permissive);
		return soup;
	}

	thread void deprecatedException(string msg){
		Interface.Exception(msg);
	}

	public void SetProperties(Soup soup){
		inherited(soup);
		Soup SignalSoup = soup.GetNamedSoup("sigdata");

		if(SignalSoup.CountTags()){
			if(soup.IsLocked()){
				Soup tmp=soup;
				soup=Constructors.NewSoup();
				soup.Copy(tmp);
			}
			int i;
			for(i=0;i<SignalSoup.CountTags();i++){//Пора и от этого избавиться!!!
				string name=SignalSoup.GetIndexedTagName(i);
				soup.SetNamedTag(name, SignalSoup.GetNamedTag(name));
			}
			deprecatedException("Old properties format detected. Please resave map and session.");
		}
		ALSNfreq = soup.GetNamedTagAsInt("ALSNfreq", FREQ_ALSN50);
		if(type&(TYPE_IN|TYPE_ROUTER|TYPE_OUT|TYPE_DIVIDE)){
			siding=soup.GetNamedTagAsInt("siding", SIDING_NONE);
			if(!(type&TYPE_IN))
				siding=siding&~SIDING_FROM;
		}
		else
			siding=SIDING_NONE;
		if(acaution_type)acaution = soup.GetNamedTagAsBool("acaution", false);
		else acaution=false;
		if(hold_mainmode)blok=(hold_mainmode==4);
		else blok = soup.GetNamedTagAsBool("blok", false);
		if(HasMesh("ta_pred"))
			predvhod = soup.GetNamedTagAsBool("predvhod", predvhod);
		else predvhod=false;
		SetMeshVisible("ta_pred", predvhod, 0.0);
		if(korBU_enable){
			korBU = soup.GetNamedTagAsInt("korBU", korBU);
			SetkorBU();
		}
		else korBU=0;
		if(freehead){
			head_rot=soup.GetNamedTagAsInt("head_rot", head_rot);
			krep_rot=soup.GetNamedTagAsInt("krep_rot", krep_rot);
			SetHeadRotation();
		}
		else
			head_rot=krep_rot=0;
		if(offset_enable)
			offset=soup.GetNamedTagAsFloat("offset", offset);
		else
			offset=base_offset;
		if(voffset_enable)
			voffset=soup.GetNamedTagAsFloat("voffset", voffset);
		else
			voffset=0;
		SetMeshTranslation("default", offset-base_offset, 0, voffset);
		speedlim1 = soup.GetNamedTagAsInt("speedlimit1", 0);
		SetName(soup.GetNamedTag("name"));
		if(type&(TYPE_IN|TYPE_ROUTER|TYPE_OUT|TYPE_DIVIDE)){
			station=soup.GetNamedTag("station");

			if(station != ""){
				string[] obj_p=new string[1];
				obj_p[0]=station;

				uzLib.LibraryCall("add_station",obj_p,null);
				obj_p[0]=null;
			}
			else{
				string default1 = uzLib.LibraryCall("station_edited_find",null,null);
				if(default1 != "")
					station=default1;
			}
		}
		else
			station="";

		//Восстановление состояния изостыка
		if(HasMesh("izo")){
			izo = soup.GetNamedTagAsBool("izo", izo);
			izo_pos = soup.GetNamedTagAsBool("izo_pos", offset > 0);
		}
		else izo=false;
		SetIzo();

		//Восстановление состояния шкафа СЦБ
		if(HasMesh("box")){
			box = soup.GetNamedTagAsBool("box", box);
			box_pos = soup.GetNamedTagAsBool("box_pos", offset > 0);
		}
		else box=false;
		SetBox();

		if(HasMesh("permissive")){
			permissive = soup.GetNamedTagAsBool("permissive", false);
			SetMeshVisible("permissive", permissive, 0.0);
		}
		else permissive=false;

		ChangeSignalEffect(GetNextState());
	}

	// Поиск сигналов
	void SignalMonitor(Message msg){
		if(msg.src == me)
			ChangeSignalEffect();
	}

	// Инициализация
	public void Init(Asset asset){
		inherited(self=asset);
		LibAsset=self.FindAsset("uzLib");
		if(!LibAsset)Interface.Exception("unable load uzLib");
		ST = LibAsset.GetStringTable();
		uzLib=World.GetLibrary(LibAsset.GetKUID());


		Soup def=self.GetConfigSoup().GetNamedSoup("extensions");
		type=def.GetNamedTagAsInt("type", 0);
		hold_mainmode=def.GetNamedTagAsInt("hold_mainmode", 0);
		predvhod=def.GetNamedTagAsBool("predvhod", false);
		acaution_type=def.GetNamedTagAsInt("acaution_type", 0);
		korBU_enable=def.GetNamedTagAsBool("korBU_enable", false);
		korBU=def.GetNamedTagAsInt("korBU_default", 0);
		if(freehead=def.GetNamedTagAsBool("freehead", false)){
			krep_rot=def.GetNamedTagAsInt("default_head_offset", 0);
			head_rot=0;
		}
		else
			head_rot=krep_rot=0;
		head_cnt=def.GetNamedTagAsInt("head_cnt", 0);
		offset_enable=def.GetNamedTagAsBool("offset_enable", false);
		base_offset=self.GetConfigSoup().GetNamedTagAsFloat("trackside", 0.0);
		offset=def.GetNamedTagAsFloat("default_offset", 0.0);
		voffset_enable=def.GetNamedTagAsBool("voffset_enable", false);
		box = def.GetNamedTagAsBool("box", false);
		izo = def.GetNamedTagAsBool("izo", false);

		ChangeSignalEffect(GetNextState());
		AddHandler(me, "Signal", "StateChanged", "SignalMonitor");
		AddHandler(me, "MapObject", "View-Details", "ViewDetails");
	}	
};