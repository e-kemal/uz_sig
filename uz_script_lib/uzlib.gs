include "Library.gs"
include "xtrainzs.gs"
include "uzsignal.gs"

class uzLib isclass Library{
	Asset self;
	BinarySortedStrings Stations;		//массив станций
	string last_edited_station="";

	public string  LibraryCall(string function, string[] stringParam, GSObject[] objectParam){
		if (function == "chain_update") {		//цепочка обновлений
			uzSignal sig0 = cast<uzSignal>objectParam[0];
			uzSignal sig = null;
			//Interface.Print("chain update for: "+sig0.GetPropertyValue("name")+" ("+sig0.GetId()+")");

			GSTrackSearch ts=sig0.BeginTrackSearch(false);
			while(ts.SearchNextObject()){
				object obj=ts.GetObject();
	//			if(cast<Vehicle>obj)break;
				if(ts.GetFacingRelativeToSearchDirection())continue;
				if(sig=cast<uzSignal>obj) {
					int oldState = sig.GetMyState();
					sig.ChangeSignalEffect(sig0.GetMyState());
					if (oldState == sig.GetMyState())
						break;
					sig0 = sig;
					sig = null;
					continue;
				}
				if(cast<Signal>obj)break;
			}
		}
		if(function=="add_station"){		// запрос на добавление станции
			if(!Stations.AddElement(stringParam[0])){
				//Interface.Exception("station "+stringParam[0]+" already exist!");
				return "false";
			}
			//Interface.Log("'"+stringParam[0]+"'");

			last_edited_station = stringParam[0];

			if((Stations.N+20) > Stations.SE.size())			// расширяем массив
				Stations.UdgradeArraySize(2*Stations.SE.size());

			return "true";
		}
		if(function=="delete_station"){		// запрос на удаление станции
			string stationnamedel = ""+stringParam[0];
			Stations.DeleteElement(stationnamedel);

			if(last_edited_station == stationnamedel)
				last_edited_station = Stations.SE[0];

			if(Stations.N>0);
			{
				string temp = Stations.SE[0];
/*
				int i;
				for(i=0;i<Signals.N;i++){
					if((cast<zxSignalLink>(Signals.DBSE[i].Object)).sign.stationName == stationnamedel)
						(cast<zxSignalLink>(Signals.DBSE[i].Object)).sign.stationName = temp + "";
				}
*/
			}
		}
		if(function=="station_exists"){		// запрос на наличие станции
			int number= Stations.Find( stringParam[0],false);
			if(number>=0){
				return "true";
			}
			return "false";
		}
		if(function=="station_list"){		// запрос на список станций
			int i;
			int size1=Stations.N;

			for(i=0;i<size1;i++)
				stringParam[i]=Stations.SE[i];

			return size1+"";
		}
		if(function=="station_count"){		// запрос на список станций
			return Stations.N+"";
		}
		if(function=="station_by_id"){		// запрос на список станций
			return Stations.SE[( Str.ToInt(stringParam[0]) )];
		}
		if(function=="station_edited_set"){		// задание редактируемой станции
			last_edited_station = stringParam[0];
			return "";
		}
		if(function=="station_edited_find"){		// задание редактируемой станции
			return last_edited_station;
		}
		return "";
	}

	void OnJunction(Message msg){
		JunctionBase JM=cast<JunctionBase>msg.src;
		if(!JM)return;
		GSTrackSearch ts=JM.BeginTrackSearch(-1);
		if(!ts)return;
		uzSignal sig=null;

		while(ts.SearchNextObject()){
			object obj=ts.GetObject();
//			if(cast<Vehicle>obj)break;
			if(ts.GetFacingRelativeToSearchDirection())continue;
			if(sig=cast<uzSignal>obj)break;
			if(cast<Signal>obj)break;
		}
		if(sig){
			sig.ChangeSignalEffect(/*nextState*/);//Все ведь понимают, что это костыль? Что при срабатывании этого события у всех нормальных людей сигнал и так закрыт.
		}
	}

	public void Init(Asset asset){
		inherited(self=asset);

		Stations=new BinarySortedStrings();
		Stations.UdgradeArraySize(20);

//		AddHandler(me, "World", "", "ModuleInitHandler");
		AddHandler(me, "Junction", "Toggled", "OnJunction");
//		AddHandler(me, "Object", "Enter", "Enter");
//		AddHandler(me, "Object", "Leave", "Leave");
//		AddHandler(me, "Train", "StartedMoving", "TrainStarting");
//		AddHandler(me, "Train", "StoppedMoving", "TrainStopping");
//		AddHandler(me, "Train", "Cleanup", "RemoveTrain");
	}
};