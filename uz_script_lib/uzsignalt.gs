// Script written by Oleg Khimich aka OlegKhim (DSemen), Camille Akhmetzyanov aka kemal, Dmitry Kovaliov aka Дмитрий К, Mikhail Rozov aka RMM, Evgeniy Varvanin aka Varz.
// Organization Trainz UP http://trainzup.com/

include "uzSignal.gs"

class uzSignalT isclass uzSignal{

	Junction[] JM=new Junction[0];
	int[] jDir=new int[0];


//	int speedlim2 = 0;
	int[] JunctID;
//	Junction[] Jlist;








	// Создание таблицы HTML
	public string GetDescriptionHTML(void){





		string ret=inherited()+"<table>";
		string bgcol=ST.GetString("BGCOLOR");
		string bgcol1=ST.GetString("BGCOLOR1");
		string bgcol2=ST.GetString("BGCOLOR2");
		string bgcol3=ST.GetString("BGCOLOR3");

		ret=ret+HTMLWindow.MakeRow(HTMLWindow.MakeCell(ST.GetString("junc_text"),bgcol)+HTMLWindow.MakeCell(MakeProperty("addjunc", "<img src='pic/add.jpg' width=84 height=24>"),bgcol2));//вариант с картинкой
		int i;
		for(i=0;i<JM.size();i++){
			ret=ret+"<tr><td "+bgcol2+"><a href='live://property/junction_"+i+"'><img src='pic/gals.jpg' width=40 height=20>";
			if(JM[i]){//мало ли
				ret=ret+JM[i].GetName()+"</a></td><td "+bgcol1+"><a href='live://property/jL_"+i+"'><img src='pic/l";
				if(jDir[i]==0)ret=ret+"-a";
				ret=ret+".tga' width=24 height=24></a>&nbsp;<a href='live://property/jR_"+i+"'><img src='pic/r";
				if(jDir[i]!=0)ret=ret+"-a";
				ret=ret+".tga' width=24 height=24></a>";
			}
			else{
				ret=ret+ST.GetString("junc_choice")+"</a></td><td "+bgcol1+"><img src='pic/l.tga' width=24 height=24>&nbsp;<img src='pic/r.tga' width=24 height=24>";
			}
			ret=ret+"&nbsp;<a href='live://property/deljunc_"+i+"'><img src='pic/delete.tga' width=24 height=24></a></td></tr>";
		}
		return ret+"</table>";
	}
/*
	public string GetContentViewDetails(void){
		string acautiontxt;
		if(acaution)
			acautiontxt = ST.GetString("mode_manevr");
		else
			acautiontxt = ST.GetString("mode_poezd");

		string ret="<HTML><body>";
		string bgcol=ST.GetString("BGCOLOR");
		string bgcol2=ST.GetString("BGCOLOR2");
		string bgcol3=ST.GetString("BGCOLOR3");
		ret=ret+HTMLWindow.StartTable();
		ret=ret+HTMLWindow.MakeRow(HTMLWindow.MakeCell(ST.GetString("text2"),bgcol)+HTMLWindow.MakeCell(HTMLWindow.MakeLink("live://property/acaution",acautiontxt),bgcol2));
		ret=ret+HTMLWindow.EndTable();
		ret=ret+"</Body></html>";
		return ret;
	}
*/




	// Создание имен свойств
	string GetPropertyName(string id){
		if(id=="addjunc")return ST.GetString("junc_choice");
		if(id[,9]=="junction_")return ST.GetString("junc_choice");
		return inherited(id);
	}

	// Создание таблиц свойств
	string GetPropertyType(string id){
		if (id=="addjunc" or id[,9]=="junction_")return "list,1";
		if (id[,8] == "deljunc_")return "link";
		if (id[,3] == "jL_")return "link";
		if (id[,3] == "jR_")return "link";
		if (id[,5] == "jDir_")return "link";
		return inherited(id);
	}

	/// Создание ссылок
	void LinkPropertyValue(string p_propertyID){
		if (p_propertyID[,8] == "deljunc_"){
			int i=Str.ToInt(p_propertyID[8,]);
			JM[i, i+1]=null;
			jDir[i, i+1]=null;
		}
		else if (p_propertyID[,3] == "jL_"){
			int i=Str.ToInt(p_propertyID[3,]);
			if(i<=jDir.size())jDir[i]=Junction.DIRECTION_LEFT;
		}
		else if (p_propertyID[,3] == "jR_"){
			int i=Str.ToInt(p_propertyID[3,]);
			if(i<=jDir.size())jDir[i]=Junction.DIRECTION_RIGHT;
		}
		else if (p_propertyID[,5] == "jDir_"){//вариант упразднён. но пусть будет...
			int i=Str.ToInt(p_propertyID[5,]);
			if(jDir[i])jDir[i]=0;
			else jDir[i]=2;
		}
/*
		else if (p_propertyID == "speedlim2_0")speedlim2 = 1;
		else if (p_propertyID == "speedlim2_1")speedlim2 = 2;
		else if (p_propertyID == "speedlim2_2")speedlim2 = 3;
		else if (p_propertyID == "speedlim2_3")speedlim2 = 4;
		else if (p_propertyID == "speedlim2_4")speedlim2 = 5;
		else if (p_propertyID == "speedlim2_5")speedlim2 = 0;
*/
		else inherited(p_propertyID);
//		ChangeSignalEffect();
	}



	public string[] GetPropertyElementList(string id){
		if (id=="addjunc" or id[,9]=="junction_"){
			int i, out = 0;
			string [] result = new string[0];
			JunctID = new int[0];
			Junction[] Jlist = World.GetJunctionList();
			for (i=0; i < Jlist.size(); ++i){
				if (Jlist[i]){
					string name = Jlist[i].GetLocalisedName();
					if (name and name.size()){
						JunctID[out] = Jlist[i].GetId();
						result[out++] = name;
					}
				}
			}
			return result;
		}
		return inherited(id);
	}

	void SetPropertyValue(string id, string value, int index){
		if(id=="addjunc"){
			int n=JM.size();
			JM[n] = cast<Junction>Router.GetGameObject(JunctID[index]);
			jDir[n] = 0;
			ChangeSignalEffect();
		}
		else if(id[,9] == "junction_"){
			int n=Str.ToInt(id[9,]);
			JM[n] = cast<Junction>Router.GetGameObject(JunctID[index]);
			jDir[n] = 0;
			ChangeSignalEffect();
		}
		else inherited(id, value, index);
	}

	void SetPropertyValue(string id, string value){
		inherited(id, value);
	}

	public string GetPropertyValue(string id){
		return inherited(id);
	}

	// Запись информации в базу данных
	public Soup GetProperties(void){
		Soup soup = inherited();


		soup.SetNamedTag("jCount", JM.size());
		int i;
		for(i=0;i<JM.size();i++){
			if(JM[i])soup.SetNamedTag("JM"+i, JM[i].GetName());
			soup.SetNamedTag("jDir"+i, jDir[i]);
		}
//		soup.SetNamedTag("speedlimit2", speedlim2);
		return soup;
	}

	// Загрузка информации из базы данных
	public void SetProperties(Soup soup){
//		soup.Log();
		Soup SignalSoup = soup.GetNamedSoup("sigdata");
		if(SignalSoup.CountTags()){
			if(soup.IsLocked()){
				Soup tmp=soup;
				soup=Constructors.NewSoup();
				soup.Copy(tmp);
			}
			int i;
			for(i=0;i<SignalSoup.CountTags();i++){//Пора и от этого избавиться!!!
				string name=SignalSoup.GetIndexedTagName(i);
				soup.SetNamedTag(name, SignalSoup.GetNamedTag(name));
			}
			soup.RemoveNamedTag("sigdata");
			deprecatedException("Old properties format detected. Please resave map and session.");
		}
//		soup.Log();


//		speedlim1 = soup.GetNamedTagAsInt("speedlimit1");
	//	speedlim2 = soup.GetNamedTagAsInt("speedlimit2");
		int i, n;
		n=soup.GetNamedTagAsInt("jCount", -1);
		JM=new Junction[0];
		jDir=new int[0];
		if(soup.GetNamedTagAsInt("direct", 42) != 42){//тогда может есть настройки в старом формате? конвертируем их
			JM[0] = cast<Junction>Router.GetGameObject(soup.GetNamedTag("mjunction"));
			bool direct = soup.GetNamedTagAsBool("direct", false);
			if(direct)jDir[0] = 2;
			else jDir[0] = 0;
			if(soup.GetNamedTagAsBool("drive", false)){
				JM[1] = cast<Junction>Router.GetGameObject(soup.GetNamedTag("addjunction"));
				if(direct)jDir[1] = 0;
				else jDir[1] = 2;
			}
			deprecatedException("Old properties format detected. Please resave map and session.");
		}
		for(i=0;i<n;i++){
			JM[i] = cast<Junction>Router.GetGameObject(soup.GetNamedTag("JM"+i));
			jDir[i] = soup.GetNamedTagAsInt("jDir"+i, 0);
		}

		inherited(soup);
//		ChangeSignalEffect(GetNextState());
	}


	bool isMRT(bool def){
		if(JM.size()==0)return def;//в этом светофоре по умолчанию отклонение
		int i;
		for(i=0;i<JM.size();i++)
			if(JM[i] and JM[i].GetDirection()==jDir[i])
				return true;
		return false;
	}
	bool isMRT(){
		return isMRT(false);
	}
/*
	// зажигание сигналов светофора в зависимости от установленных параметров
	void ChangeSignalEffect(int NextState) {
		int SigState = me.GetSignalStateEx();
		me.SetSpeedLimit(0.0);
		blens="";

		SetMeshVisible("yellow", false, 0.4);
		SetMeshVisible("red", false, 0.4);
		SetMeshVisible("green4", false, 0.4);
		SetMeshVisible("red4", false, 0.4);
		SetMeshVisible("green", false, 0.4);
		SetMeshVisible("yellow2", false, 0.4);
		SetMeshVisible("white", false, 0.4);
		SetMeshVisible("arrow0", false, 0.4);
		SetMeshVisible("arrow1", false, 0.4);

		if (SigState==EX_STOP){
			if(blok)
				SetMeshVisible("red4", true, 0.9);
			else
				SetMeshVisible("red", true, 0.9);
			ALSNcode=CODE_REDYELLOW;
			state=SS_COUNT0;
			return;//nothing more to do here
		}
		if(acaution){
			SetMeshVisible("white", true, 0.9);blens="white";blinker();
			if(blok)
				SetMeshVisible("red4", true, 0.9);
			else
				SetMeshVisible("red", true, 0.9);
			if(speedlim1 == 1)
				me.SetSpeedLimit(5.65);
			ALSNcode=CODE_REDYELLOW;
			state=SS_COUNT0;
			return;
		}
		if(isMRT()){
			SetMeshVisible("yellow2", true, 0.9);
			SetMeshVisible("yellow", true, 0.9);//на отклонение в любом случае два жёлтых
			if(SigState==EX_PROCEED or SigState==EX_ADVANCE_CAUTION){//Если же впереди свободны 2 БУ, то достаточно лишь включить мигалку
				blens="yellow";
				blinker();
			}
			switch(speedlim2){
				case 1:SetSpeedLimit(6.95);break;
				case 2:SetSpeedLimit(11.1);break;
				case 3:SetSpeedLimit(13.95);break;
				case 4:SetSpeedLimit(19.4);break;
				case 5:SetSpeedLimit(22.25);break;
				default:SetSpeedLimit(0.0);break;
			}
			ALSNcode=CODE_YELLOW;
			state=SS_COUNT1|SS_MRT;
			return;
		}
		if(korBU==1)SetMeshVisible("arrow0", true, 0.9);
		if(korBU==2)SetMeshVisible("arrow1", true, 0.9);
		if (SigState==EX_CAUTION){
			SetMeshVisible("yellow", true, 0.9);
			ALSNcode=CODE_YELLOW;
			state=SS_COUNT1;
			return;
		}
		ALSNcode=CODE_GREEN;
		if(NextState&SS_MRT){
			SetMeshVisible("yellow", true, 0.9);
			blens="yellow";
			blinker();
			state=SS_COUNT1;
			return;
		}
		if(NextState&SS_MRT18){
			if(blok)
				blens="green4";
			else
				blens="green";
			SetMeshVisible(blens, true, 0.9);
			blinker();
			state=SS_COUNT2;
			return;
		}
		if (blok and (NextState&SS_COUNT)<SS_COUNT2){
			SetMeshVisible("green4", true, 0.9);
			SetMeshVisible("yellow", true, 0.9);
			state=SS_COUNT2;
			return;
		}
		state=SS_COUNT3;
		if(blok)
			SetMeshVisible("green4", true, 0.9);
		else
			SetMeshVisible("green", true, 0.9);
	}*/
	// Инициализация
	public void Init(Asset asset){
		JM=new Junction[0];
		jDir=new int[0];
		inherited(asset);
	}
};