// Script written by Oleg Khimich aka OlegKhim (DSemen), Camille Akhmetzyanov aka kemal, Dmitry Kovaliov aka Дмитрий К, Mikhail Rozov aka RMM, Evgeniy Varvanin aka Varz. 
// Organization Trainz UP http://trainzup.com/

include "uzSignal.gs"

class uzSigPro isclass uzSignal {

	void SetName(string value){
		inherited(value);
		string[] chars=GetChars(value);
		int i;
		for(i=0;i<5;i++){
			int n = i;
			if (predvhod)
				n = n - 1;
			if(n>= 0 and n < chars.size() and chars[n]!=""){
				SetFXNameText("name"+i, chars[n]);
				SetMeshVisible("ta"+i, true, 0.0);
			}
			else{
				SetFXNameText("name"+i, " ");
				SetMeshVisible("ta"+i, false, 0.0);
			}
		}
	}

	// зажигание сигналов светофора
	void ChangeSignalEffect(int NextState) {
		int SigState = me.GetSignalStateEx();
		SetSpeedLimit(0.0);
		blens="";
		state = state & ~(SS_COUNT | SS_MRT | SS_MRT18);
		SetMeshVisible("yellow", false, 0.4);
		SetMeshVisible("red", false, 0.4);
		SetMeshVisible("green4", false, 0.4);
		SetMeshVisible("red4", false, 0.4);
		SetMeshVisible("green", false, 0.4);
		SetMeshVisible("arrow0", false, 0.4);
		SetMeshVisible("arrow1", false, 0.4);

		if (SigState == EX_STOP or SigState == EX_STOP_THEN_CONTINUE){
			if(blok)
				SetMeshVisible("red4", true, 0.9);
			else
				SetMeshVisible("red", true, 0.9);
			ALSNcode=CODE_REDYELLOW;
			state = state | SS_COUNT0;
			return;//nothing more to do here
		}
		if(korBU==1)SetMeshVisible("arrow0", true, 0.9);
		if(korBU==2)SetMeshVisible("arrow1", true, 0.9);
		if(SigState==EX_CAUTION){
			SetMeshVisible("yellow", true, 0.9);
			ALSNcode=CODE_YELLOW;
			state = state | SS_COUNT1;
			return;
		}
		ALSNcode=CODE_GREEN;
		if(NextState&SS_MRT){
			SetMeshVisible("yellow", true, 0.9);
			blens="yellow";
			blinker();
			state = state | SS_COUNT1;
			return;
		}
		if(NextState&SS_MRT18){
			if(blok)
				blens="green4";
			else
				blens="green";
			SetMeshVisible(blens, true, 0.9);
			blinker();
			state = state | SS_COUNT2;
			return;
		}
		if(blok and (NextState&SS_COUNT)<SS_COUNT2){
			SetMeshVisible("green4", true, 0.9);
			SetMeshVisible("yellow", true, 0.9);
			state = state | SS_COUNT2;
			return;
		}
		state = state | SS_COUNT3;
		if(blok)
			SetMeshVisible("green4", true, 0.9);
		else
			SetMeshVisible("green", true, 0.9);
	}
};