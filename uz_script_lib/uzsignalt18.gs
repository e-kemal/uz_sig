// Script written by Oleg Khimich aka OlegKhim (DSemen), Camille Akhmetzyanov aka kemal, Dmitry Kovaliov aka Дмитрий К, Mikhail Rozov aka RMM, Evgeniy Varvanin aka Varz.
// Organization Trainz UP http://trainzup.com/

include "uzSignal.gs"
/*
class uzT18path{
	public Junction bJM;
	public int bjDir;
	public Junction[] JM=new Junction[0];
	public int[] jDir=new int[0];
};
*/
class uzPathBranch{
	public Junction[] JM=new Junction[0];
	public int[] type=new int[0];
	public define int J_DIRECTION_MASK	= 7;
	public define int J_MRT		= 8;
	public define int J_MRT18	= 16;
	public int[] index=new int[0];
};

class uzSignalT18 isclass uzSignal{
//	uzT18path[] JM18=new uzT18path[0];
	uzPathBranch[] Paths=new uzPathBranch[0];
	int[] JunctID;

	string GetPathHTML(int index, string prefix){
		if(index<0 or index>=Paths.size())
			return "";
		int i;
		string ret = "<tr><td colspan=\"2\">" + prefix + "<a href='live://property/pathaddjunc_" + index + "'>add</a>&nbsp;<a href=\"live://property/delbranch_" + index + "\">del</a></td></tr>";
		for(i=0;i<Paths[index].JM.size();i++){
			ret = ret + "<tr><td>";
			ret = ret + prefix + i + ". <a href='live://property/pathjunction_" + index + "_" + i + "'>";
			if(Paths[index].JM[i])
				ret=ret+Paths[index].JM[i].GetName();
			else
				ret=ret+ST.GetString("junc_choice");
			ret=ret+"</a>";
			if (Paths[index].index[i] < 0)
				ret = ret + "&nbsp;<a href='live://property/addbranch_" + index + "_" + i + "'>Add Branch</a>";
			ret = ret + "</td><td>";
			ret=ret+"dir: <a href='live://property/pathjL_"+index+"_"+i+"'><img src='pic/object/ar-l_";
			if((Paths[index].type[i]&uzPathBranch.J_DIRECTION_MASK)==0)ret=ret+"on";
			else ret = ret + "off";
			ret=ret+".tga' width=24 height=24></a>&nbsp;<a href='live://property/pathjR_"+index+"_"+i+"'><img src='pic/object/ar-r_";
			if((Paths[index].type[i]&uzPathBranch.J_DIRECTION_MASK)!=0)ret=ret+"on";
			else ret = ret + "off";
			ret=ret+".tga' width=24 height=24></a>, type: "+(Paths[index].type[i]&~uzPathBranch.J_DIRECTION_MASK)+" <a href=\"live://property/jtype_"+index+"_"+i+"_"+uzPathBranch.J_MRT+"\">MRT ";
			if(Paths[index].type[i]&uzPathBranch.J_MRT)ret=ret+"+";
			else ret=ret+"-";
			ret=ret+"</a>, <a href=\"live://property/jtype_"+index+"_"+i+"_"+uzPathBranch.J_MRT18+"\">MRT18 ";
			if(Paths[index].type[i]&uzPathBranch.J_MRT18)ret=ret+"+";
			else ret=ret+"-";
			ret=ret+"</a>, <a href=\"live://property/deljunc_" + index + "_" + i + "\">del</a>";
			ret=ret+"</td></tr>";
			if (Paths[index].index[i] > 0)
				ret = ret + GetPathHTML(Paths[index].index[i], prefix + ".&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
		}
		return ret;
	}

	public string GetCntDesctiption(void){
		string ret = inherited();
		ret = ret + GetPathHTML(0, "");
		return ret;
	}


	// Создание имен свойств
	string GetPropertyName(string id){
		if(id=="addjunc")return ST.GetString("junc_choice");
		if(id[,12]=="pathaddjunc_")return ST.GetString("junc_choice");
		if(id[,9]=="junction_")return ST.GetString("junc_choice");
		if(id[,13]=="pathjunction_")return ST.GetString("junc_choice");
		return inherited(id);
	}

	// Создание таблиц свойств
	string GetPropertyType(string id){
		if (id == "addjunc" or id[,12] == "pathaddjunc_" or id[,9] == "junction_" or id[,13] == "pathjunction_")return "list,1";
		if (id[,10] == "addbranch_") return "link";
		if (id[,10] == "delbranch_") return "link";
		if (id[,8] == "deljunc_") return "link";
		if (id[,3] == "jL_") return "link";
		if (id[,7] == "pathjL_") return "link";
		if (id[,3] == "jR_") return "link";
		if (id[,7] == "pathjR_") return "link";
		if (id[,5] == "jDir_") return "link";
		if (id[,6] == "jtype_") return "link";
		return inherited(id);
	}

	void delBranch(int n) {
		if (n < 1 or n >= Paths.size()) {
			return;
		}
		int i;
		for (i = 0; i < Paths[n].index.size(); ++i) {
			if (Paths[n].index[i] > 0) {
				delBranch(Paths[n].index[i]);
//				Paths[n].index[i] = -1;
			}
		}
		Paths[n, n + 1] = null;
		for (i = 0; i < Paths.size(); ++i) {
			int j;
			for (j = 0; j < Paths[i].index.size(); ++j) {
				if (Paths[i].index[j] == n) {
					Paths[i].index[j] = -1;
				}
				else if (Paths[i].index[j] > n) {
					Paths[i].index[j] = Paths[i].index[j] - 1;
				}
			}
		}
	}

	/// Создание ссылок
	void LinkPropertyValue(string id){
		if(id[,10]=="addbranch_"){
			string[] str=Str.Tokens(id[10,], "_");
			if(str.size()<2)return;
			int n=Str.ToInt(str[0]);
			int m=Str.ToInt(str[1]);
			if(Paths.size()<=n)return;
			if(Paths[n].index.size()<=m)return;
			if(Paths[n].index[m]>0)return;
			int k=Paths.size();
			Paths[k]=new uzPathBranch();
			Paths[n].index[m]=k;
		}
		else if (id[,10] == "delbranch_") {
			delBranch(Str.ToInt(id[10,]));
			ChangeSignalEffect();
		}
		else if (id[,8] == "deljunc_"){
			string[] str = Str.Tokens(id[8,], "_");
			if (str.size() < 2) return;
			int n = Str.ToInt(str[0]);
			int m = Str.ToInt(str[1]);
			if (Paths.size() <= n) return;
			if (Paths[n].JM.size() <= m) return;
			if (Paths[n].index[m] > 0) {
				delBranch(Paths[n].index[m]);
			}
			Paths[n].JM[m, m + 1] = null;
			Paths[n].type[m, m + 1] = null;
			Paths[n].index[m, m + 1] = null;
			ChangeSignalEffect();
		}
		else if(id[,7] == "pathjL_"){
			string[] str=Str.Tokens(id[7,], "_");
			if(str.size()<2)return;
			int n=Str.ToInt(str[0]);
			int m=Str.ToInt(str[1]);
			if(Paths.size()<=n)return;
			if(Paths[n].JM.size()<=m)return;
			Paths[n].type[m]=(Paths[n].type[m]&~uzPathBranch.J_DIRECTION_MASK)|(Junction.DIRECTION_LEFT&uzPathBranch.J_DIRECTION_MASK);
			ChangeSignalEffect();
		}
		else if(id[,7] == "pathjR_"){
			string[] str=Str.Tokens(id[7,], "_");
			if(str.size()<2)return;
			int n=Str.ToInt(str[0]);
			int m=Str.ToInt(str[1]);
			if(Paths.size()<=n)return;
			if(Paths[n].JM.size()<=m)return;
			Paths[n].type[m]=(Paths[n].type[m]&~uzPathBranch.J_DIRECTION_MASK)|(Junction.DIRECTION_RIGHT&uzPathBranch.J_DIRECTION_MASK);
			ChangeSignalEffect();
		}
		else if(id[,6] == "jtype_"){
			string[] str=Str.Tokens(id[6,], "_");
			if(str.size()<3)return;
			int n=Str.ToInt(str[0]);
			int m=Str.ToInt(str[1]);
			int k=Str.ToInt(str[2]);
			if(Paths.size()<=n)return;
			if(Paths[n].type.size()<=m)return;
			Paths[n].type[m]=Paths[n].type[m]^k;
			ChangeSignalEffect();
		}
/*
		else if (id == "speedlim2_0")speedlim2 = 1;
		else if (id == "speedlim2_1")speedlim2 = 2;
		else if (id == "speedlim2_2")speedlim2 = 3;
		else if (id == "speedlim2_3")speedlim2 = 4;
		else if (id == "speedlim2_4")speedlim2 = 5;
		else if (id == "speedlim2_5")speedlim2 = 0;
*/
		else inherited(id);
	}



	public string[] GetPropertyElementList(string id){
		if (id=="addjunc" or id[,12]=="pathaddjunc_" or id[,9]=="junction_" or id[,13]=="pathjunction_"){
			int i, out = 0;
			string [] result = new string[0];
			JunctID = new int[0];
			Junction[] Jlist = World.GetJunctionList();
			for (i=0; i < Jlist.size(); ++i){
				if (Jlist[i]){
					string name = Jlist[i].GetLocalisedName();
					if (name and name.size()){
						JunctID[out] = Jlist[i].GetId();
						result[out++] = name;
					}
				}
			}
			return result;
		}
		return inherited(id);
	}

	void SetPropertyValue(string id, string value, int index){
		if(id[,12]=="pathaddjunc_"){
			int n=Str.ToInt(id[12,]);
			if(Paths.size()<=n)return;
			int m=Paths[n].JM.size();
			Paths[n].JM[m]=cast<Junction>Router.GetGameObject(JunctID[index]);
			Paths[n].type[m]=0;
			Paths[n].index[m]=-1;
			ChangeSignalEffect();
		}
		else if(id[,13] == "pathjunction_"){
			string[] str=Str.Tokens(id[13,], "_");
			if(str.size()<2)return;
			int n=Str.ToInt(str[0]);
			int m=Str.ToInt(str[1]);
			if(Paths.size()<=n)return;
			if(Paths[n].JM.size()<=m)return;
			Paths[n].JM[m]=cast<Junction>Router.GetGameObject(JunctID[index]);
			Paths[n].type[m]=0;
			ChangeSignalEffect();
		}
		else inherited(id, value, index);
	}

	void SetPropertyValue(string id, string value){
		inherited(id, value);
	}

	public string GetPropertyValue(string id){
		return inherited(id);
	}

	// Запись информации в базу данных
	public Soup GetProperties(void){
		Soup soup = inherited();

		soup.SetNamedTag("branchcount", Paths.size());
		int i;
		for(i=0;i<Paths.size();i++){
			soup.SetNamedTag("branchjcount."+i, Paths[i].JM.size());
			int j;
			for(j=0;j<Paths[i].JM.size();j++){
				if(Paths[i].JM[j])soup.SetNamedTag("branchjm."+i+"."+j, Paths[i].JM[j].GetName());
				soup.SetNamedTag("branchjtype."+i+"."+j, Paths[i].type[j]);
				soup.SetNamedTag("branchchild."+i+"."+j, Paths[i].index[j]);
			}
		}
		return soup;
	}

	// Загрузка информации из базы данных
	public void SetProperties(Soup soup){
		Soup SignalSoup = soup.GetNamedSoup("sigdata");
		if(SignalSoup.CountTags()){
			if(soup.IsLocked()){
				Soup tmp=soup;
				soup=Constructors.NewSoup();
				soup.Copy(tmp);
			}
			int i;
			for(i=0;i<SignalSoup.CountTags();i++){//Пора и от этого избавиться!!!
				string name=SignalSoup.GetIndexedTagName(i);
				soup.SetNamedTag(name, SignalSoup.GetNamedTag(name));
			}
			soup.RemoveNamedTag("sigdata");
			deprecatedException("Old properties format detected. Please resave map and session.");
		}


		Paths=new uzPathBranch[0];
		int i, j, n, m;
		n=soup.GetNamedTagAsInt("branchcount", -1);
		if(n<0){//тогда может есть настройки в старом формате? конвертируем их
			Paths[0]=new uzPathBranch();

//			Paths[0].JM=new Junction[0];
//			Paths[0].type=new int[0];
//			Paths[0].index=new int[0];
			n=soup.GetNamedTagAsInt("jCount", -1);
			if(soup.GetNamedTagAsInt("direct", 42) != 42){//тогда может есть настройки в более старом формате? конвертируем их
				Paths[0].JM[0] = cast<Junction>Router.GetGameObject(soup.GetNamedTag("mjunction"));
				bool direct = soup.GetNamedTagAsBool("direct", false);
				if(direct)Paths[0].type[0] = 2|uzPathBranch.J_MRT18;
				else Paths[0].type[0] = 0|uzPathBranch.J_MRT18;
				Paths[0].index[0] = -1;
				if(soup.GetNamedTagAsBool("drive", false)){
					Paths[0].JM[1] = cast<Junction>Router.GetGameObject(soup.GetNamedTag("addjunction"));
					if(direct)Paths[0].type[1] = 0|uzPathBranch.J_MRT18;
					else Paths[0].type[1] = 2|uzPathBranch.J_MRT18;
					Paths[0].index[1] = -1;
				}
				deprecatedException("Old properties format detected. Please resave map and session.");
			}
			for(i=0;i<n;i++){
				Paths[0].JM[i] = cast<Junction>Router.GetGameObject(soup.GetNamedTag("JM"+i));
				Paths[0].type[i]=soup.GetNamedTagAsInt("jDir"+i, 0)|uzPathBranch.J_MRT18;
				Paths[0].index[i]=-1;
			}
		}
		else{
			for(i=0;i<n;i++){
				Paths[i]=new uzPathBranch();
				m=soup.GetNamedTagAsInt("branchjcount."+i, -1);
				for(j=0;j<m;j++){
					Paths[i].JM[j] = cast<Junction>Router.GetGameObject(soup.GetNamedTag("branchjm."+i+"."+j));
					Paths[i].type[j]=soup.GetNamedTagAsInt("branchjtype."+i+"."+j, 0);
					Paths[i].index[j]=soup.GetNamedTagAsInt("branchchild."+i+"."+j, 0);
				}
			}
		}
		if(Paths.size()<1)
			Paths[0]=new uzPathBranch();

		inherited(soup);
//		ChangeSignalEffect(GetNextState());
	}

/*
	// Поиск сигналов
	void SignalMonitor(Message msg){
		if(msg.major == "Signal")
			if(cast<Signal>msg.src == me) ChangeSignalEffect(GetNextState());

		if(msg.major == "Junction"){
			me.SetSignalState(AUTOMATIC,"");
			ChangeSignalEffect(GetNextState());
		}
	}
*/

	int CheckMRT(int index, int def){
		if(index<0 or index>=Paths.size())return def;
		int i;
		for(i=0;i<Paths[index].JM.size();i++)
			if(Paths[index].JM[i] and Paths[index].JM[i].GetDirection()==(Paths[index].type[i]&uzPathBranch.J_DIRECTION_MASK)){
				def=def|Paths[index].type[i];
				if(Paths[index].index[i]>-1)
					def=def|CheckMRT(Paths[index].index[i], 0);
				return def;
			}
		return def;
	}
/*
	bool isMRT(bool def){
		if(JM.size()==0)return def;//в этом светофоре по умолчанию отклонение
		int i;
		for(i=0;i<JM.size();i++)
			if(JM[i] and JM[i].GetDirection()==jDir[i])
				return true;
		return false;
	}
	bool isMRT(){
		return isMRT(false);
	}
*/
	// Инициализация
	public void Init(Asset asset){
	//	JM=new Junction[0];
	//	jDir=new int[0];
		inherited(asset);
	}
};
